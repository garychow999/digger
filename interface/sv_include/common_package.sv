`ifndef COMMON_PACKAGE
`define COMMON_PACKAGE


`define VLD_WIDTH                       1
`define TYPE_WIDTH                      8

// typedef
//=============================================================================
typedef logic[0:0]                      vld_t;
typedef logic[7:0]                      type_t;


`endif
