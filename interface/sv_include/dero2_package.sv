`ifndef DERO2_PACKAGE
`define DERO2_PACKAGE

`include "dero_package.sv"

// typedef
//=============================================================================
typedef collector_t[15:0]_col_mux_in_t;

// struct
//=============================================================================
typedef struct packed {
    _col_mux_in_t                       data;
} col_mux_in_t;



`endif
