`ifndef KECCAK_PACKAGE
`define KECCAK_PACKAGE

`include "common_package.sv"

`define KECCAK_BE_WIDTH                 8
`define KECCAK_DATA_WIDTH               1088

// typedef
//=============================================================================
typedef logic[7:0]                      keccak_be_t;
typedef logic[1087:0]                   keccak_data_t;

// struct
//=============================================================================
typedef struct packed {
    vld_t                               vld;
    vld_t                               last;
    keccak_be_t                         be;
    keccak_data_t                       data;
} keccak_t;



`endif
