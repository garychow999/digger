`ifndef DERO_PACKAGE
`define DERO_PACKAGE

`include "common_package.sv"

`define SORTER_KEY_WIDTH                44
`define SORTER_DATA_WIDTH               8
`define SORTER_AW                       12
`define MEM_ID_WIDTH                    3
`define MUX_LAYER_NUM                   7
`define SORTER_INT_KEY_WIDTH            37
`define COLLECTOR_BE_WIDTH              4
`define COLLECTOR_BW                    16
`define COLLECTOR_WIDTH                 128
`define COL_MUX_AW                      8
`define COLLECTOR_GROUP_AW              4
`define COLLECTOR_GROUP                 16

// typedef
//=============================================================================
typedef logic[43:0]                     sorter_key_t;
typedef logic[36:0]                     sorter_int_key_t;
typedef logic[7:0]                      sorter_data_t;
typedef logic[11:0]                     sorter_addr_t;
typedef logic[2:0]                      mem_id_t;
typedef logic[127:0]                    collector_data_t;
typedef logic[3:0]                      collector_be_t;
typedef logic[7:0]                      col_mux_addr_t;
typedef logic[3:0]                      collector_id_t;

// struct
//=============================================================================
typedef struct packed {
    vld_t                               vld;
    sorter_data_t                       data;
    sorter_key_t                        key;
} sorter_key_data_t;

typedef struct packed {
    sorter_data_t                       data;
    sorter_int_key_t                    key;
} sorter_int_key_data_t;

typedef struct packed {
    sorter_addr_t                       ub;
    sorter_addr_t                       lb;
} stack_t;

typedef struct packed {
    vld_t                               vld;
    vld_t                               both;
    sorter_int_key_data_t               data;
    sorter_addr_t                       addr;
} mem_wcmd_t;

typedef struct packed {
    vld_t                               vld;
    vld_t                               pump;
    stack_t                             init_addrs;
    mem_id_t                            id;
    sorter_addr_t                       addr;
} mem_rcmd_t;

typedef struct packed {
    mem_rcmd_t                          rcmd;
    mem_wcmd_t                          wcmd;
} mem_cmd_t;

typedef struct packed {
    vld_t                               vld;
    stack_t                             init_addrs;
    sorter_int_key_data_t               data;
    mem_id_t                            id;
} mem_data_t;

typedef struct packed {
    vld_t                               vld;
    sorter_data_t                       data;
} mem_pump_data_t;

typedef struct packed {
    vld_t                               pump_done;
    vld_t                               pump_start;
    vld_t                               accept_input;
} sorter_ctrl_out_t;

typedef struct packed {
    vld_t                               vld;
    collector_be_t                      be;
    collector_data_t                    data;
    collector_id_t                      id;
} collector_t;

typedef struct packed {
    vld_t                               pump_col_mux;
    vld_t                               reset_col_mux;
} main_ctrl_t;



`endif
