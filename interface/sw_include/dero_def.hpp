#pragma once

#define SORTER_KEY_WIDTH                44
#define SORTER_DATA_WIDTH               8
#define SORTER_AW                       12
#define MEM_ID_WIDTH                    3
#define MUX_LAYER_NUM                   7
#define SORTER_INT_KEY_WIDTH            (SORTER_KEY_WIDTH - MUX_LAYER_NUM)
#define COLLECTOR_BE_WIDTH              4
#define COLLECTOR_BW                    (1 << COLLECTOR_BE_WIDTH)
#define COLLECTOR_WIDTH                 (COLLECTOR_BW * 8)
#define COL_MUX_AW                      (SORTER_AW - COLLECTOR_BE_WIDTH)
#define COLLECTOR_GROUP_AW              4
#define COLLECTOR_GROUP                 (1 << COLLECTOR_GROUP_AW)

