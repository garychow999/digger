#pragma once

#pragma pack(push, 1)

#include <dero_def.hpp>

#include <cstdint>

#include <common_sw.hpp>

typedef uint64_t sorter_key_sw_t;
typedef uint64_t sorter_int_key_sw_t;
typedef uint8_t sorter_data_sw_t;
typedef uint16_t sorter_addr_sw_t;
typedef uint8_t mem_id_sw_t;
typedef char collector_data_sw_t[16];
typedef uint8_t collector_be_sw_t;
typedef uint8_t col_mux_addr_sw_t;
typedef uint8_t collector_id_sw_t;



struct sorter_key_data_sw_t {
	sorter_key_sw_t key;
	sorter_data_sw_t data;
	vld_sw_t vld;
};

struct sorter_int_key_data_sw_t {
	sorter_int_key_sw_t key;
	sorter_data_sw_t data;
};

struct stack_sw_t {
	sorter_addr_sw_t lb;
	sorter_addr_sw_t ub;
};

struct mem_wcmd_sw_t {
	sorter_addr_sw_t addr;
	sorter_int_key_data_sw_t data;
	vld_sw_t both;
	vld_sw_t vld;
};

struct mem_rcmd_sw_t {
	sorter_addr_sw_t addr;
	mem_id_sw_t id;
	stack_sw_t init_addrs;
	vld_sw_t pump;
	vld_sw_t vld;
};

struct mem_cmd_sw_t {
	mem_wcmd_sw_t wcmd;
	mem_rcmd_sw_t rcmd;
};

struct mem_data_sw_t {
	mem_id_sw_t id;
	sorter_int_key_data_sw_t data;
	stack_sw_t init_addrs;
	vld_sw_t vld;
};

struct mem_pump_data_sw_t {
	sorter_data_sw_t data;
	vld_sw_t vld;
};

struct sorter_ctrl_out_sw_t {
	vld_sw_t accept_input;
	vld_sw_t pump_start;
	vld_sw_t pump_done;
};

struct collector_sw_t {
	collector_id_sw_t id;
	collector_data_sw_t data;
	collector_be_sw_t be;
	vld_sw_t vld;
};

struct main_ctrl_sw_t {
	vld_sw_t reset_col_mux;
	vld_sw_t pump_col_mux;
};

#pragma pack(pop)

