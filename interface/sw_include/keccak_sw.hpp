#pragma once

#pragma pack(push, 1)

#include <keccak_def.hpp>

#include <cstdint>

#include <common_sw.hpp>

typedef uint8_t keccak_be_sw_t;
typedef char keccak_data_sw_t[136];



struct keccak_sw_t {
	keccak_data_sw_t data;
	keccak_be_sw_t be;
	vld_sw_t last;
	vld_sw_t vld;
};

#pragma pack(pop)

