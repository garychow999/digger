#pragma once

// magic
#define API_MAGIC                       0x1

#pragma pack(push, 1)

#include <top_def.hpp>

#include <cstdint>

#include <common_sw.hpp>
#include <dero_sw.hpp>
#include <dero2_sw.hpp>
#include <keccak_sw.hpp>

#pragma pack(pop)

