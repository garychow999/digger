#pragma once

#pragma pack(push, 1)

#include <dero2_def.hpp>

#include <cstdint>

#include <dero_sw.hpp>

typedef collector_sw_t _col_mux_in_sw_t[COLLECTOR_GROUP];



struct col_mux_in_sw_t {
	_col_mux_in_sw_t data;
};

#pragma pack(pop)

