#pragma once

#include <common.hpp>

// include the def from sw_include
#include <keccak_def.hpp>

// HLS typedef
//=============================================================================
typedef U8                              keccak_be_t;
typedef U1088                           keccak_data_t;
//=============================================================================

// sw_t typedef
//=============================================================================
typedef U8 keccak_be_sw_t;
typedef U1088 keccak_data_sw_t;
//=============================================================================

// IO type
//=============================================================================
typedef ap_uint<1098> keccak_io_t;
//=============================================================================

// HLS struct forward declaration
//=============================================================================
struct keccak_t;
struct keccak_sw_t;

struct keccak_t {
    static const int bitwidth = 1098;

    keccak_data_t                       data;
    keccak_be_t                         be;
    vld_t                               last;
    vld_t                               vld;

    keccak_t operator~() const;
    keccak_t operator&(const keccak_t& t) const;
    keccak_t operator|(const keccak_t& t) const;
    keccak_t operator^(const keccak_t& t) const;

    ap_uint<1098> bits() const;
    static keccak_t zeros();
    static keccak_t ones();

    keccak_t& operator=(const keccak_io_t& t);
    keccak_t& operator=(const keccak_sw_t& t);
};

struct keccak_sw_t {

    keccak_data_sw_t data;
    keccak_be_sw_t be;
    vld_sw_t last;
    vld_sw_t vld;

    ap_uint<1112> bits() const;
    keccak_sw_t& operator=(const keccak_t& t);
    keccak_sw_t& operator=(const ap_uint<4096>& t);
};

//=============================================================================

// HLS struct impl
//=============================================================================
inline keccak_t keccak_t::operator~() const
{
    keccak_t ret;
    ret.data = ~(this->data);
    ret.be = ~(this->be);
    ret.last = ~(this->last);
    ret.vld = ~(this->vld);
    return ret;
}

inline keccak_t keccak_t::operator&(const keccak_t& t) const
{
    keccak_t ret;
    ret.data = (this->data) & t.data;
    ret.be = (this->be) & t.be;
    ret.last = (this->last) & t.last;
    ret.vld = (this->vld) & t.vld;
    return ret;
}

inline keccak_t keccak_t::operator|(const keccak_t& t) const
{
    keccak_t ret;
    ret.data = (this->data) | t.data;
    ret.be = (this->be) | t.be;
    ret.last = (this->last) | t.last;
    ret.vld = (this->vld) | t.vld;
    return ret;
}

inline keccak_t keccak_t::operator^(const keccak_t& t) const
{
    keccak_t ret;
    ret.data = (this->data) ^ t.data;
    ret.be = (this->be) ^ t.be;
    ret.last = (this->last) ^ t.last;
    ret.vld = (this->vld) ^ t.vld;
    return ret;
}

inline ap_uint<1098> keccak_t::bits() const
{
    ap_uint<1098> ret;
    ret(1087, 0) = data;
    ret(1095, 1088) = be;
    ret(1096, 1096) = last;
    ret(1097, 1097) = vld;
    return ret;
}

inline keccak_t keccak_t::zeros()
{
    keccak_t ret;
    ret.data = 0;
    ret.be = 0;
    ret.last = 0;
    ret.vld = 0;
    return ret;
}

inline keccak_t keccak_t::ones()
{
    keccak_t ret;
    ret.data = -1;
    ret.be = -1;
    ret.last = -1;
    ret.vld = -1;
    return ret;
}

inline keccak_t& keccak_t::operator=(const keccak_io_t& t)
{
    this->data = t(1087, 0);
    this->be = t(1095, 1088);
    this->last = t(1096, 1096);
    this->vld = t(1097, 1097);
    return *this;
}

inline keccak_t& keccak_t::operator=(const keccak_sw_t& t)
{
    this->data = t.data;
    this->be = t.be;
    this->last = t.last;
    this->vld = t.vld;
    return *this;
}

inline ap_uint<1112> keccak_sw_t::bits() const
{
    ap_uint<1112> ret;
    ret(1087, 0) = data;
    ret(1095, 1088) = be;
    ret(1103, 1096) = last;
    ret(1111, 1104) = vld;
    return ret;
}

inline keccak_sw_t& keccak_sw_t::operator=(const keccak_t& t)
{
    this->data = t.data;
    this->be = t.be;
    this->last = t.last;
    this->vld = t.vld;
    return *this;
}

inline keccak_sw_t& keccak_sw_t::operator=(const ap_uint<4096>& t)
{
    this->data = t(1087, 0);
    this->be = t(1095, 1088);
    this->last = t(1103, 1096);
    this->vld = t(1111, 1104);
    return *this;
}


