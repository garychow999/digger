#pragma once

#include <dero.hpp>

// include the def from sw_include
#include <dero2_def.hpp>

// HLS typedef
//=============================================================================
typedef collector_t                     _col_mux_in_t[COLLECTOR_GROUP];
//=============================================================================

// sw_t typedef
//=============================================================================
typedef collector_sw_t _col_mux_in_sw_t [COLLECTOR_GROUP];
//=============================================================================

// IO type
//=============================================================================
typedef ap_uint<2192> col_mux_in_io_t;
//=============================================================================

// HLS struct forward declaration
//=============================================================================
struct col_mux_in_t;
struct col_mux_in_sw_t;

struct col_mux_in_t {
    static const int bitwidth = 2192;

    _col_mux_in_t                       data;

    col_mux_in_t operator~() const;
    col_mux_in_t operator&(const col_mux_in_t& t) const;
    col_mux_in_t operator|(const col_mux_in_t& t) const;
    col_mux_in_t operator^(const col_mux_in_t& t) const;

    ap_uint<2192> bits() const;
    static col_mux_in_t zeros();
    static col_mux_in_t ones();

    col_mux_in_t& operator=(const col_mux_in_io_t& t);
    col_mux_in_t& operator=(const col_mux_in_sw_t& t);
};

struct col_mux_in_sw_t {

    _col_mux_in_sw_t data;

    ap_uint<2432> bits() const;
    col_mux_in_sw_t& operator=(const col_mux_in_t& t);
    col_mux_in_sw_t& operator=(const ap_uint<4096>& t);
};

//=============================================================================

// HLS struct impl
//=============================================================================
inline col_mux_in_t col_mux_in_t::operator~() const
{
    col_mux_in_t ret;
    for(int i=0; i<16; i++)
        ret.data[i] = ~(this->data[i]);
    return ret;
}

inline col_mux_in_t col_mux_in_t::operator&(const col_mux_in_t& t) const
{
    col_mux_in_t ret;
    for(int i=0; i<16; i++)
        ret.data[i] = (this->data[i]) & (t.data[i]);
    return ret;
}

inline col_mux_in_t col_mux_in_t::operator|(const col_mux_in_t& t) const
{
    col_mux_in_t ret;
    for(int i=0; i<16; i++)
        ret.data[i] = (this->data[i]) | (t.data[i]);
    return ret;
}

inline col_mux_in_t col_mux_in_t::operator^(const col_mux_in_t& t) const
{
    col_mux_in_t ret;
    for(int i=0; i<16; i++)
        ret.data[i] = (this->data[i]) ^ (t.data[i]);
    return ret;
}

inline ap_uint<2192> col_mux_in_t::bits() const
{
    ap_uint<2192> ret;
    for(int i=0; i<16; i++)
        ret(0+(i+1)*137-1,0+(i)*137) = data[i].bits();
    return ret;
}

inline col_mux_in_t col_mux_in_t::zeros()
{
    col_mux_in_t ret;
    for(int i=0; i<16; i++)
        ret.data[i] = 0;
    return ret;
}

inline col_mux_in_t col_mux_in_t::ones()
{
    col_mux_in_t ret;
    for(int i=0; i<16; i++)
        ret.data[i] = -1;
    return ret;
}

inline col_mux_in_t& col_mux_in_t::operator=(const col_mux_in_io_t& t)
{
    for(int i=0; i<16; i++)
        this->data[i] = t(0+(i+1)*137-1,0+(i)*137);
    return *this;
}

inline col_mux_in_t& col_mux_in_t::operator=(const col_mux_in_sw_t& t)
{
    for(int i=0; i<16; i++)
        this->data[i] = t.data[i];
    return *this;
}

inline ap_uint<2432> col_mux_in_sw_t::bits() const
{
    ap_uint<2432> ret;
    for(int i=0; i<16; i++)
        ret(0+(i+1)*152-1,0+(i)*152) = data[i].bits();
    return ret;
}

inline col_mux_in_sw_t& col_mux_in_sw_t::operator=(const col_mux_in_t& t)
{
    for(int i=0; i<16; i++)
        this->data[i] = t.data[i];
    return *this;
}

inline col_mux_in_sw_t& col_mux_in_sw_t::operator=(const ap_uint<4096>& t)
{
    for(int i=0; i<16; i++)
        this->data[i] = t(0+(i+1)*152-1,0+(i)*152);
    return *this;
}


