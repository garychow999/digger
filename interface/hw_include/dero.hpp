#pragma once

#include <common.hpp>

// include the def from sw_include
#include <dero_def.hpp>

// HLS typedef
//=============================================================================
typedef U44                             sorter_key_t;
typedef U37                             sorter_int_key_t;
typedef U8                              sorter_data_t;
typedef U12                             sorter_addr_t;
typedef U3                              mem_id_t;
typedef U128                            collector_data_t;
typedef U4                              collector_be_t;
typedef U8                              col_mux_addr_t;
typedef U4                              collector_id_t;
//=============================================================================

// sw_t typedef
//=============================================================================
typedef U64 sorter_key_sw_t;
typedef U64 sorter_int_key_sw_t;
typedef U8 sorter_data_sw_t;
typedef U16 sorter_addr_sw_t;
typedef U8 mem_id_sw_t;
typedef U128 collector_data_sw_t;
typedef U8 collector_be_sw_t;
typedef U8 col_mux_addr_sw_t;
typedef U8 collector_id_sw_t;
//=============================================================================

// IO type
//=============================================================================
typedef ap_uint<53> sorter_key_data_io_t;
typedef ap_uint<45> sorter_int_key_data_io_t;
typedef ap_uint<24> stack_io_t;
typedef ap_uint<59> mem_wcmd_io_t;
typedef ap_uint<41> mem_rcmd_io_t;
typedef ap_uint<100> mem_cmd_io_t;
typedef ap_uint<73> mem_data_io_t;
typedef ap_uint<9> mem_pump_data_io_t;
typedef ap_uint<3> sorter_ctrl_out_io_t;
typedef ap_uint<137> collector_io_t;
typedef ap_uint<2> main_ctrl_io_t;
//=============================================================================

// HLS struct forward declaration
//=============================================================================
struct sorter_key_data_t;
struct sorter_key_data_sw_t;

struct sorter_key_data_t {
    static const int bitwidth = 53;

    sorter_key_t                        key;
    sorter_data_t                       data;
    vld_t                               vld;

    sorter_key_data_t operator~() const;
    sorter_key_data_t operator&(const sorter_key_data_t& t) const;
    sorter_key_data_t operator|(const sorter_key_data_t& t) const;
    sorter_key_data_t operator^(const sorter_key_data_t& t) const;

    ap_uint<53> bits() const;
    static sorter_key_data_t zeros();
    static sorter_key_data_t ones();

    sorter_key_data_t& operator=(const sorter_key_data_io_t& t);
    sorter_key_data_t& operator=(const sorter_key_data_sw_t& t);
};

struct sorter_key_data_sw_t {

    sorter_key_sw_t key;
    sorter_data_sw_t data;
    vld_sw_t vld;

    ap_uint<80> bits() const;
    sorter_key_data_sw_t& operator=(const sorter_key_data_t& t);
    sorter_key_data_sw_t& operator=(const ap_uint<4096>& t);
};

struct sorter_int_key_data_t;
struct sorter_int_key_data_sw_t;

struct sorter_int_key_data_t {
    static const int bitwidth = 45;

    sorter_int_key_t                    key;
    sorter_data_t                       data;

    sorter_int_key_data_t operator~() const;
    sorter_int_key_data_t operator&(const sorter_int_key_data_t& t) const;
    sorter_int_key_data_t operator|(const sorter_int_key_data_t& t) const;
    sorter_int_key_data_t operator^(const sorter_int_key_data_t& t) const;

    ap_uint<45> bits() const;
    static sorter_int_key_data_t zeros();
    static sorter_int_key_data_t ones();

    sorter_int_key_data_t& operator=(const sorter_int_key_data_io_t& t);
    sorter_int_key_data_t& operator=(const sorter_int_key_data_sw_t& t);
};

struct sorter_int_key_data_sw_t {

    sorter_int_key_sw_t key;
    sorter_data_sw_t data;

    ap_uint<72> bits() const;
    sorter_int_key_data_sw_t& operator=(const sorter_int_key_data_t& t);
    sorter_int_key_data_sw_t& operator=(const ap_uint<4096>& t);
};

struct stack_t;
struct stack_sw_t;

struct stack_t {
    static const int bitwidth = 24;

    sorter_addr_t                       lb;
    sorter_addr_t                       ub;

    stack_t operator~() const;
    stack_t operator&(const stack_t& t) const;
    stack_t operator|(const stack_t& t) const;
    stack_t operator^(const stack_t& t) const;

    ap_uint<24> bits() const;
    static stack_t zeros();
    static stack_t ones();

    stack_t& operator=(const stack_io_t& t);
    stack_t& operator=(const stack_sw_t& t);
};

struct stack_sw_t {

    sorter_addr_sw_t lb;
    sorter_addr_sw_t ub;

    ap_uint<32> bits() const;
    stack_sw_t& operator=(const stack_t& t);
    stack_sw_t& operator=(const ap_uint<4096>& t);
};

struct mem_wcmd_t;
struct mem_wcmd_sw_t;

struct mem_wcmd_t {
    static const int bitwidth = 59;

    sorter_addr_t                       addr;
    sorter_int_key_data_t               data;
    vld_t                               both;
    vld_t                               vld;

    mem_wcmd_t operator~() const;
    mem_wcmd_t operator&(const mem_wcmd_t& t) const;
    mem_wcmd_t operator|(const mem_wcmd_t& t) const;
    mem_wcmd_t operator^(const mem_wcmd_t& t) const;

    ap_uint<59> bits() const;
    static mem_wcmd_t zeros();
    static mem_wcmd_t ones();

    mem_wcmd_t& operator=(const mem_wcmd_io_t& t);
    mem_wcmd_t& operator=(const mem_wcmd_sw_t& t);
};

struct mem_wcmd_sw_t {

    sorter_addr_sw_t addr;
    sorter_int_key_data_sw_t data;
    vld_sw_t both;
    vld_sw_t vld;

    ap_uint<104> bits() const;
    mem_wcmd_sw_t& operator=(const mem_wcmd_t& t);
    mem_wcmd_sw_t& operator=(const ap_uint<4096>& t);
};

struct mem_rcmd_t;
struct mem_rcmd_sw_t;

struct mem_rcmd_t {
    static const int bitwidth = 41;

    sorter_addr_t                       addr;
    mem_id_t                            id;
    stack_t                             init_addrs;
    vld_t                               pump;
    vld_t                               vld;

    mem_rcmd_t operator~() const;
    mem_rcmd_t operator&(const mem_rcmd_t& t) const;
    mem_rcmd_t operator|(const mem_rcmd_t& t) const;
    mem_rcmd_t operator^(const mem_rcmd_t& t) const;

    ap_uint<41> bits() const;
    static mem_rcmd_t zeros();
    static mem_rcmd_t ones();

    mem_rcmd_t& operator=(const mem_rcmd_io_t& t);
    mem_rcmd_t& operator=(const mem_rcmd_sw_t& t);
};

struct mem_rcmd_sw_t {

    sorter_addr_sw_t addr;
    mem_id_sw_t id;
    stack_sw_t init_addrs;
    vld_sw_t pump;
    vld_sw_t vld;

    ap_uint<72> bits() const;
    mem_rcmd_sw_t& operator=(const mem_rcmd_t& t);
    mem_rcmd_sw_t& operator=(const ap_uint<4096>& t);
};

struct mem_cmd_t;
struct mem_cmd_sw_t;

struct mem_cmd_t {
    static const int bitwidth = 100;

    mem_wcmd_t                          wcmd;
    mem_rcmd_t                          rcmd;

    mem_cmd_t operator~() const;
    mem_cmd_t operator&(const mem_cmd_t& t) const;
    mem_cmd_t operator|(const mem_cmd_t& t) const;
    mem_cmd_t operator^(const mem_cmd_t& t) const;

    ap_uint<100> bits() const;
    static mem_cmd_t zeros();
    static mem_cmd_t ones();

    mem_cmd_t& operator=(const mem_cmd_io_t& t);
    mem_cmd_t& operator=(const mem_cmd_sw_t& t);
};

struct mem_cmd_sw_t {

    mem_wcmd_sw_t wcmd;
    mem_rcmd_sw_t rcmd;

    ap_uint<176> bits() const;
    mem_cmd_sw_t& operator=(const mem_cmd_t& t);
    mem_cmd_sw_t& operator=(const ap_uint<4096>& t);
};

struct mem_data_t;
struct mem_data_sw_t;

struct mem_data_t {
    static const int bitwidth = 73;

    mem_id_t                            id;
    sorter_int_key_data_t               data;
    stack_t                             init_addrs;
    vld_t                               vld;

    mem_data_t operator~() const;
    mem_data_t operator&(const mem_data_t& t) const;
    mem_data_t operator|(const mem_data_t& t) const;
    mem_data_t operator^(const mem_data_t& t) const;

    ap_uint<73> bits() const;
    static mem_data_t zeros();
    static mem_data_t ones();

    mem_data_t& operator=(const mem_data_io_t& t);
    mem_data_t& operator=(const mem_data_sw_t& t);
};

struct mem_data_sw_t {

    mem_id_sw_t id;
    sorter_int_key_data_sw_t data;
    stack_sw_t init_addrs;
    vld_sw_t vld;

    ap_uint<120> bits() const;
    mem_data_sw_t& operator=(const mem_data_t& t);
    mem_data_sw_t& operator=(const ap_uint<4096>& t);
};

struct mem_pump_data_t;
struct mem_pump_data_sw_t;

struct mem_pump_data_t {
    static const int bitwidth = 9;

    sorter_data_t                       data;
    vld_t                               vld;

    mem_pump_data_t operator~() const;
    mem_pump_data_t operator&(const mem_pump_data_t& t) const;
    mem_pump_data_t operator|(const mem_pump_data_t& t) const;
    mem_pump_data_t operator^(const mem_pump_data_t& t) const;

    ap_uint<9> bits() const;
    static mem_pump_data_t zeros();
    static mem_pump_data_t ones();

    mem_pump_data_t& operator=(const mem_pump_data_io_t& t);
    mem_pump_data_t& operator=(const mem_pump_data_sw_t& t);
};

struct mem_pump_data_sw_t {

    sorter_data_sw_t data;
    vld_sw_t vld;

    ap_uint<16> bits() const;
    mem_pump_data_sw_t& operator=(const mem_pump_data_t& t);
    mem_pump_data_sw_t& operator=(const ap_uint<4096>& t);
};

struct sorter_ctrl_out_t;
struct sorter_ctrl_out_sw_t;

struct sorter_ctrl_out_t {
    static const int bitwidth = 3;

    vld_t                               accept_input;
    vld_t                               pump_start;
    vld_t                               pump_done;

    sorter_ctrl_out_t operator~() const;
    sorter_ctrl_out_t operator&(const sorter_ctrl_out_t& t) const;
    sorter_ctrl_out_t operator|(const sorter_ctrl_out_t& t) const;
    sorter_ctrl_out_t operator^(const sorter_ctrl_out_t& t) const;

    ap_uint<3> bits() const;
    static sorter_ctrl_out_t zeros();
    static sorter_ctrl_out_t ones();

    sorter_ctrl_out_t& operator=(const sorter_ctrl_out_io_t& t);
    sorter_ctrl_out_t& operator=(const sorter_ctrl_out_sw_t& t);
};

struct sorter_ctrl_out_sw_t {

    vld_sw_t accept_input;
    vld_sw_t pump_start;
    vld_sw_t pump_done;

    ap_uint<24> bits() const;
    sorter_ctrl_out_sw_t& operator=(const sorter_ctrl_out_t& t);
    sorter_ctrl_out_sw_t& operator=(const ap_uint<4096>& t);
};

struct collector_t;
struct collector_sw_t;

struct collector_t {
    static const int bitwidth = 137;

    collector_id_t                      id;
    collector_data_t                    data;
    collector_be_t                      be;
    vld_t                               vld;

    collector_t operator~() const;
    collector_t operator&(const collector_t& t) const;
    collector_t operator|(const collector_t& t) const;
    collector_t operator^(const collector_t& t) const;

    ap_uint<137> bits() const;
    static collector_t zeros();
    static collector_t ones();

    collector_t& operator=(const collector_io_t& t);
    collector_t& operator=(const collector_sw_t& t);
};

struct collector_sw_t {

    collector_id_sw_t id;
    collector_data_sw_t data;
    collector_be_sw_t be;
    vld_sw_t vld;

    ap_uint<152> bits() const;
    collector_sw_t& operator=(const collector_t& t);
    collector_sw_t& operator=(const ap_uint<4096>& t);
};

struct main_ctrl_t;
struct main_ctrl_sw_t;

struct main_ctrl_t {
    static const int bitwidth = 2;

    vld_t                               reset_col_mux;
    vld_t                               pump_col_mux;

    main_ctrl_t operator~() const;
    main_ctrl_t operator&(const main_ctrl_t& t) const;
    main_ctrl_t operator|(const main_ctrl_t& t) const;
    main_ctrl_t operator^(const main_ctrl_t& t) const;

    ap_uint<2> bits() const;
    static main_ctrl_t zeros();
    static main_ctrl_t ones();

    main_ctrl_t& operator=(const main_ctrl_io_t& t);
    main_ctrl_t& operator=(const main_ctrl_sw_t& t);
};

struct main_ctrl_sw_t {

    vld_sw_t reset_col_mux;
    vld_sw_t pump_col_mux;

    ap_uint<16> bits() const;
    main_ctrl_sw_t& operator=(const main_ctrl_t& t);
    main_ctrl_sw_t& operator=(const ap_uint<4096>& t);
};

//=============================================================================

// HLS struct impl
//=============================================================================
inline sorter_key_data_t sorter_key_data_t::operator~() const
{
    sorter_key_data_t ret;
    ret.key = ~(this->key);
    ret.data = ~(this->data);
    ret.vld = ~(this->vld);
    return ret;
}

inline sorter_key_data_t sorter_key_data_t::operator&(const sorter_key_data_t& t) const
{
    sorter_key_data_t ret;
    ret.key = (this->key) & t.key;
    ret.data = (this->data) & t.data;
    ret.vld = (this->vld) & t.vld;
    return ret;
}

inline sorter_key_data_t sorter_key_data_t::operator|(const sorter_key_data_t& t) const
{
    sorter_key_data_t ret;
    ret.key = (this->key) | t.key;
    ret.data = (this->data) | t.data;
    ret.vld = (this->vld) | t.vld;
    return ret;
}

inline sorter_key_data_t sorter_key_data_t::operator^(const sorter_key_data_t& t) const
{
    sorter_key_data_t ret;
    ret.key = (this->key) ^ t.key;
    ret.data = (this->data) ^ t.data;
    ret.vld = (this->vld) ^ t.vld;
    return ret;
}

inline ap_uint<53> sorter_key_data_t::bits() const
{
    ap_uint<53> ret;
    ret(43, 0) = key;
    ret(51, 44) = data;
    ret(52, 52) = vld;
    return ret;
}

inline sorter_key_data_t sorter_key_data_t::zeros()
{
    sorter_key_data_t ret;
    ret.key = 0;
    ret.data = 0;
    ret.vld = 0;
    return ret;
}

inline sorter_key_data_t sorter_key_data_t::ones()
{
    sorter_key_data_t ret;
    ret.key = -1;
    ret.data = -1;
    ret.vld = -1;
    return ret;
}

inline sorter_key_data_t& sorter_key_data_t::operator=(const sorter_key_data_io_t& t)
{
    this->key = t(43, 0);
    this->data = t(51, 44);
    this->vld = t(52, 52);
    return *this;
}

inline sorter_key_data_t& sorter_key_data_t::operator=(const sorter_key_data_sw_t& t)
{
    this->key = t.key;
    this->data = t.data;
    this->vld = t.vld;
    return *this;
}

inline ap_uint<80> sorter_key_data_sw_t::bits() const
{
    ap_uint<80> ret;
    ret(63, 0) = key;
    ret(71, 64) = data;
    ret(79, 72) = vld;
    return ret;
}

inline sorter_key_data_sw_t& sorter_key_data_sw_t::operator=(const sorter_key_data_t& t)
{
    this->key = t.key;
    this->data = t.data;
    this->vld = t.vld;
    return *this;
}

inline sorter_key_data_sw_t& sorter_key_data_sw_t::operator=(const ap_uint<4096>& t)
{
    this->key = t(63, 0);
    this->data = t(71, 64);
    this->vld = t(79, 72);
    return *this;
}

inline sorter_int_key_data_t sorter_int_key_data_t::operator~() const
{
    sorter_int_key_data_t ret;
    ret.key = ~(this->key);
    ret.data = ~(this->data);
    return ret;
}

inline sorter_int_key_data_t sorter_int_key_data_t::operator&(const sorter_int_key_data_t& t) const
{
    sorter_int_key_data_t ret;
    ret.key = (this->key) & t.key;
    ret.data = (this->data) & t.data;
    return ret;
}

inline sorter_int_key_data_t sorter_int_key_data_t::operator|(const sorter_int_key_data_t& t) const
{
    sorter_int_key_data_t ret;
    ret.key = (this->key) | t.key;
    ret.data = (this->data) | t.data;
    return ret;
}

inline sorter_int_key_data_t sorter_int_key_data_t::operator^(const sorter_int_key_data_t& t) const
{
    sorter_int_key_data_t ret;
    ret.key = (this->key) ^ t.key;
    ret.data = (this->data) ^ t.data;
    return ret;
}

inline ap_uint<45> sorter_int_key_data_t::bits() const
{
    ap_uint<45> ret;
    ret(36, 0) = key;
    ret(44, 37) = data;
    return ret;
}

inline sorter_int_key_data_t sorter_int_key_data_t::zeros()
{
    sorter_int_key_data_t ret;
    ret.key = 0;
    ret.data = 0;
    return ret;
}

inline sorter_int_key_data_t sorter_int_key_data_t::ones()
{
    sorter_int_key_data_t ret;
    ret.key = -1;
    ret.data = -1;
    return ret;
}

inline sorter_int_key_data_t& sorter_int_key_data_t::operator=(const sorter_int_key_data_io_t& t)
{
    this->key = t(36, 0);
    this->data = t(44, 37);
    return *this;
}

inline sorter_int_key_data_t& sorter_int_key_data_t::operator=(const sorter_int_key_data_sw_t& t)
{
    this->key = t.key;
    this->data = t.data;
    return *this;
}

inline ap_uint<72> sorter_int_key_data_sw_t::bits() const
{
    ap_uint<72> ret;
    ret(63, 0) = key;
    ret(71, 64) = data;
    return ret;
}

inline sorter_int_key_data_sw_t& sorter_int_key_data_sw_t::operator=(const sorter_int_key_data_t& t)
{
    this->key = t.key;
    this->data = t.data;
    return *this;
}

inline sorter_int_key_data_sw_t& sorter_int_key_data_sw_t::operator=(const ap_uint<4096>& t)
{
    this->key = t(63, 0);
    this->data = t(71, 64);
    return *this;
}

inline stack_t stack_t::operator~() const
{
    stack_t ret;
    ret.lb = ~(this->lb);
    ret.ub = ~(this->ub);
    return ret;
}

inline stack_t stack_t::operator&(const stack_t& t) const
{
    stack_t ret;
    ret.lb = (this->lb) & t.lb;
    ret.ub = (this->ub) & t.ub;
    return ret;
}

inline stack_t stack_t::operator|(const stack_t& t) const
{
    stack_t ret;
    ret.lb = (this->lb) | t.lb;
    ret.ub = (this->ub) | t.ub;
    return ret;
}

inline stack_t stack_t::operator^(const stack_t& t) const
{
    stack_t ret;
    ret.lb = (this->lb) ^ t.lb;
    ret.ub = (this->ub) ^ t.ub;
    return ret;
}

inline ap_uint<24> stack_t::bits() const
{
    ap_uint<24> ret;
    ret(11, 0) = lb;
    ret(23, 12) = ub;
    return ret;
}

inline stack_t stack_t::zeros()
{
    stack_t ret;
    ret.lb = 0;
    ret.ub = 0;
    return ret;
}

inline stack_t stack_t::ones()
{
    stack_t ret;
    ret.lb = -1;
    ret.ub = -1;
    return ret;
}

inline stack_t& stack_t::operator=(const stack_io_t& t)
{
    this->lb = t(11, 0);
    this->ub = t(23, 12);
    return *this;
}

inline stack_t& stack_t::operator=(const stack_sw_t& t)
{
    this->lb = t.lb;
    this->ub = t.ub;
    return *this;
}

inline ap_uint<32> stack_sw_t::bits() const
{
    ap_uint<32> ret;
    ret(15, 0) = lb;
    ret(31, 16) = ub;
    return ret;
}

inline stack_sw_t& stack_sw_t::operator=(const stack_t& t)
{
    this->lb = t.lb;
    this->ub = t.ub;
    return *this;
}

inline stack_sw_t& stack_sw_t::operator=(const ap_uint<4096>& t)
{
    this->lb = t(15, 0);
    this->ub = t(31, 16);
    return *this;
}

inline mem_wcmd_t mem_wcmd_t::operator~() const
{
    mem_wcmd_t ret;
    ret.addr = ~(this->addr);
    ret.data = ~(this->data);
    ret.both = ~(this->both);
    ret.vld = ~(this->vld);
    return ret;
}

inline mem_wcmd_t mem_wcmd_t::operator&(const mem_wcmd_t& t) const
{
    mem_wcmd_t ret;
    ret.addr = (this->addr) & t.addr;
    ret.data = (this->data) & t.data;
    ret.both = (this->both) & t.both;
    ret.vld = (this->vld) & t.vld;
    return ret;
}

inline mem_wcmd_t mem_wcmd_t::operator|(const mem_wcmd_t& t) const
{
    mem_wcmd_t ret;
    ret.addr = (this->addr) | t.addr;
    ret.data = (this->data) | t.data;
    ret.both = (this->both) | t.both;
    ret.vld = (this->vld) | t.vld;
    return ret;
}

inline mem_wcmd_t mem_wcmd_t::operator^(const mem_wcmd_t& t) const
{
    mem_wcmd_t ret;
    ret.addr = (this->addr) ^ t.addr;
    ret.data = (this->data) ^ t.data;
    ret.both = (this->both) ^ t.both;
    ret.vld = (this->vld) ^ t.vld;
    return ret;
}

inline ap_uint<59> mem_wcmd_t::bits() const
{
    ap_uint<59> ret;
    ret(11, 0) = addr;
    ret(56, 12) = data.bits();
    ret(57, 57) = both;
    ret(58, 58) = vld;
    return ret;
}

inline mem_wcmd_t mem_wcmd_t::zeros()
{
    mem_wcmd_t ret;
    ret.addr = 0;
    ret.data = sorter_int_key_data_t::zeros();
    ret.both = 0;
    ret.vld = 0;
    return ret;
}

inline mem_wcmd_t mem_wcmd_t::ones()
{
    mem_wcmd_t ret;
    ret.addr = -1;
    ret.data = sorter_int_key_data_t::ones();
    ret.both = -1;
    ret.vld = -1;
    return ret;
}

inline mem_wcmd_t& mem_wcmd_t::operator=(const mem_wcmd_io_t& t)
{
    this->addr = t(11, 0);
    this->data = t(56, 12);
    this->both = t(57, 57);
    this->vld = t(58, 58);
    return *this;
}

inline mem_wcmd_t& mem_wcmd_t::operator=(const mem_wcmd_sw_t& t)
{
    this->addr = t.addr;
    this->data = t.data;
    this->both = t.both;
    this->vld = t.vld;
    return *this;
}

inline ap_uint<104> mem_wcmd_sw_t::bits() const
{
    ap_uint<104> ret;
    ret(15, 0) = addr;
    ret(87, 16) = data.bits();
    ret(95, 88) = both;
    ret(103, 96) = vld;
    return ret;
}

inline mem_wcmd_sw_t& mem_wcmd_sw_t::operator=(const mem_wcmd_t& t)
{
    this->addr = t.addr;
    this->data = t.data;
    this->both = t.both;
    this->vld = t.vld;
    return *this;
}

inline mem_wcmd_sw_t& mem_wcmd_sw_t::operator=(const ap_uint<4096>& t)
{
    this->addr = t(15, 0);
    this->data = t(87, 16);
    this->both = t(95, 88);
    this->vld = t(103, 96);
    return *this;
}

inline mem_rcmd_t mem_rcmd_t::operator~() const
{
    mem_rcmd_t ret;
    ret.addr = ~(this->addr);
    ret.id = ~(this->id);
    ret.init_addrs = ~(this->init_addrs);
    ret.pump = ~(this->pump);
    ret.vld = ~(this->vld);
    return ret;
}

inline mem_rcmd_t mem_rcmd_t::operator&(const mem_rcmd_t& t) const
{
    mem_rcmd_t ret;
    ret.addr = (this->addr) & t.addr;
    ret.id = (this->id) & t.id;
    ret.init_addrs = (this->init_addrs) & t.init_addrs;
    ret.pump = (this->pump) & t.pump;
    ret.vld = (this->vld) & t.vld;
    return ret;
}

inline mem_rcmd_t mem_rcmd_t::operator|(const mem_rcmd_t& t) const
{
    mem_rcmd_t ret;
    ret.addr = (this->addr) | t.addr;
    ret.id = (this->id) | t.id;
    ret.init_addrs = (this->init_addrs) | t.init_addrs;
    ret.pump = (this->pump) | t.pump;
    ret.vld = (this->vld) | t.vld;
    return ret;
}

inline mem_rcmd_t mem_rcmd_t::operator^(const mem_rcmd_t& t) const
{
    mem_rcmd_t ret;
    ret.addr = (this->addr) ^ t.addr;
    ret.id = (this->id) ^ t.id;
    ret.init_addrs = (this->init_addrs) ^ t.init_addrs;
    ret.pump = (this->pump) ^ t.pump;
    ret.vld = (this->vld) ^ t.vld;
    return ret;
}

inline ap_uint<41> mem_rcmd_t::bits() const
{
    ap_uint<41> ret;
    ret(11, 0) = addr;
    ret(14, 12) = id;
    ret(38, 15) = init_addrs.bits();
    ret(39, 39) = pump;
    ret(40, 40) = vld;
    return ret;
}

inline mem_rcmd_t mem_rcmd_t::zeros()
{
    mem_rcmd_t ret;
    ret.addr = 0;
    ret.id = 0;
    ret.init_addrs = stack_t::zeros();
    ret.pump = 0;
    ret.vld = 0;
    return ret;
}

inline mem_rcmd_t mem_rcmd_t::ones()
{
    mem_rcmd_t ret;
    ret.addr = -1;
    ret.id = -1;
    ret.init_addrs = stack_t::ones();
    ret.pump = -1;
    ret.vld = -1;
    return ret;
}

inline mem_rcmd_t& mem_rcmd_t::operator=(const mem_rcmd_io_t& t)
{
    this->addr = t(11, 0);
    this->id = t(14, 12);
    this->init_addrs = t(38, 15);
    this->pump = t(39, 39);
    this->vld = t(40, 40);
    return *this;
}

inline mem_rcmd_t& mem_rcmd_t::operator=(const mem_rcmd_sw_t& t)
{
    this->addr = t.addr;
    this->id = t.id;
    this->init_addrs = t.init_addrs;
    this->pump = t.pump;
    this->vld = t.vld;
    return *this;
}

inline ap_uint<72> mem_rcmd_sw_t::bits() const
{
    ap_uint<72> ret;
    ret(15, 0) = addr;
    ret(23, 16) = id;
    ret(55, 24) = init_addrs.bits();
    ret(63, 56) = pump;
    ret(71, 64) = vld;
    return ret;
}

inline mem_rcmd_sw_t& mem_rcmd_sw_t::operator=(const mem_rcmd_t& t)
{
    this->addr = t.addr;
    this->id = t.id;
    this->init_addrs = t.init_addrs;
    this->pump = t.pump;
    this->vld = t.vld;
    return *this;
}

inline mem_rcmd_sw_t& mem_rcmd_sw_t::operator=(const ap_uint<4096>& t)
{
    this->addr = t(15, 0);
    this->id = t(23, 16);
    this->init_addrs = t(55, 24);
    this->pump = t(63, 56);
    this->vld = t(71, 64);
    return *this;
}

inline mem_cmd_t mem_cmd_t::operator~() const
{
    mem_cmd_t ret;
    ret.wcmd = ~(this->wcmd);
    ret.rcmd = ~(this->rcmd);
    return ret;
}

inline mem_cmd_t mem_cmd_t::operator&(const mem_cmd_t& t) const
{
    mem_cmd_t ret;
    ret.wcmd = (this->wcmd) & t.wcmd;
    ret.rcmd = (this->rcmd) & t.rcmd;
    return ret;
}

inline mem_cmd_t mem_cmd_t::operator|(const mem_cmd_t& t) const
{
    mem_cmd_t ret;
    ret.wcmd = (this->wcmd) | t.wcmd;
    ret.rcmd = (this->rcmd) | t.rcmd;
    return ret;
}

inline mem_cmd_t mem_cmd_t::operator^(const mem_cmd_t& t) const
{
    mem_cmd_t ret;
    ret.wcmd = (this->wcmd) ^ t.wcmd;
    ret.rcmd = (this->rcmd) ^ t.rcmd;
    return ret;
}

inline ap_uint<100> mem_cmd_t::bits() const
{
    ap_uint<100> ret;
    ret(58, 0) = wcmd.bits();
    ret(99, 59) = rcmd.bits();
    return ret;
}

inline mem_cmd_t mem_cmd_t::zeros()
{
    mem_cmd_t ret;
    ret.wcmd = mem_wcmd_t::zeros();
    ret.rcmd = mem_rcmd_t::zeros();
    return ret;
}

inline mem_cmd_t mem_cmd_t::ones()
{
    mem_cmd_t ret;
    ret.wcmd = mem_wcmd_t::ones();
    ret.rcmd = mem_rcmd_t::ones();
    return ret;
}

inline mem_cmd_t& mem_cmd_t::operator=(const mem_cmd_io_t& t)
{
    this->wcmd = t(58, 0);
    this->rcmd = t(99, 59);
    return *this;
}

inline mem_cmd_t& mem_cmd_t::operator=(const mem_cmd_sw_t& t)
{
    this->wcmd = t.wcmd;
    this->rcmd = t.rcmd;
    return *this;
}

inline ap_uint<176> mem_cmd_sw_t::bits() const
{
    ap_uint<176> ret;
    ret(103, 0) = wcmd.bits();
    ret(175, 104) = rcmd.bits();
    return ret;
}

inline mem_cmd_sw_t& mem_cmd_sw_t::operator=(const mem_cmd_t& t)
{
    this->wcmd = t.wcmd;
    this->rcmd = t.rcmd;
    return *this;
}

inline mem_cmd_sw_t& mem_cmd_sw_t::operator=(const ap_uint<4096>& t)
{
    this->wcmd = t(103, 0);
    this->rcmd = t(175, 104);
    return *this;
}

inline mem_data_t mem_data_t::operator~() const
{
    mem_data_t ret;
    ret.id = ~(this->id);
    ret.data = ~(this->data);
    ret.init_addrs = ~(this->init_addrs);
    ret.vld = ~(this->vld);
    return ret;
}

inline mem_data_t mem_data_t::operator&(const mem_data_t& t) const
{
    mem_data_t ret;
    ret.id = (this->id) & t.id;
    ret.data = (this->data) & t.data;
    ret.init_addrs = (this->init_addrs) & t.init_addrs;
    ret.vld = (this->vld) & t.vld;
    return ret;
}

inline mem_data_t mem_data_t::operator|(const mem_data_t& t) const
{
    mem_data_t ret;
    ret.id = (this->id) | t.id;
    ret.data = (this->data) | t.data;
    ret.init_addrs = (this->init_addrs) | t.init_addrs;
    ret.vld = (this->vld) | t.vld;
    return ret;
}

inline mem_data_t mem_data_t::operator^(const mem_data_t& t) const
{
    mem_data_t ret;
    ret.id = (this->id) ^ t.id;
    ret.data = (this->data) ^ t.data;
    ret.init_addrs = (this->init_addrs) ^ t.init_addrs;
    ret.vld = (this->vld) ^ t.vld;
    return ret;
}

inline ap_uint<73> mem_data_t::bits() const
{
    ap_uint<73> ret;
    ret(2, 0) = id;
    ret(47, 3) = data.bits();
    ret(71, 48) = init_addrs.bits();
    ret(72, 72) = vld;
    return ret;
}

inline mem_data_t mem_data_t::zeros()
{
    mem_data_t ret;
    ret.id = 0;
    ret.data = sorter_int_key_data_t::zeros();
    ret.init_addrs = stack_t::zeros();
    ret.vld = 0;
    return ret;
}

inline mem_data_t mem_data_t::ones()
{
    mem_data_t ret;
    ret.id = -1;
    ret.data = sorter_int_key_data_t::ones();
    ret.init_addrs = stack_t::ones();
    ret.vld = -1;
    return ret;
}

inline mem_data_t& mem_data_t::operator=(const mem_data_io_t& t)
{
    this->id = t(2, 0);
    this->data = t(47, 3);
    this->init_addrs = t(71, 48);
    this->vld = t(72, 72);
    return *this;
}

inline mem_data_t& mem_data_t::operator=(const mem_data_sw_t& t)
{
    this->id = t.id;
    this->data = t.data;
    this->init_addrs = t.init_addrs;
    this->vld = t.vld;
    return *this;
}

inline ap_uint<120> mem_data_sw_t::bits() const
{
    ap_uint<120> ret;
    ret(7, 0) = id;
    ret(79, 8) = data.bits();
    ret(111, 80) = init_addrs.bits();
    ret(119, 112) = vld;
    return ret;
}

inline mem_data_sw_t& mem_data_sw_t::operator=(const mem_data_t& t)
{
    this->id = t.id;
    this->data = t.data;
    this->init_addrs = t.init_addrs;
    this->vld = t.vld;
    return *this;
}

inline mem_data_sw_t& mem_data_sw_t::operator=(const ap_uint<4096>& t)
{
    this->id = t(7, 0);
    this->data = t(79, 8);
    this->init_addrs = t(111, 80);
    this->vld = t(119, 112);
    return *this;
}

inline mem_pump_data_t mem_pump_data_t::operator~() const
{
    mem_pump_data_t ret;
    ret.data = ~(this->data);
    ret.vld = ~(this->vld);
    return ret;
}

inline mem_pump_data_t mem_pump_data_t::operator&(const mem_pump_data_t& t) const
{
    mem_pump_data_t ret;
    ret.data = (this->data) & t.data;
    ret.vld = (this->vld) & t.vld;
    return ret;
}

inline mem_pump_data_t mem_pump_data_t::operator|(const mem_pump_data_t& t) const
{
    mem_pump_data_t ret;
    ret.data = (this->data) | t.data;
    ret.vld = (this->vld) | t.vld;
    return ret;
}

inline mem_pump_data_t mem_pump_data_t::operator^(const mem_pump_data_t& t) const
{
    mem_pump_data_t ret;
    ret.data = (this->data) ^ t.data;
    ret.vld = (this->vld) ^ t.vld;
    return ret;
}

inline ap_uint<9> mem_pump_data_t::bits() const
{
    ap_uint<9> ret;
    ret(7, 0) = data;
    ret(8, 8) = vld;
    return ret;
}

inline mem_pump_data_t mem_pump_data_t::zeros()
{
    mem_pump_data_t ret;
    ret.data = 0;
    ret.vld = 0;
    return ret;
}

inline mem_pump_data_t mem_pump_data_t::ones()
{
    mem_pump_data_t ret;
    ret.data = -1;
    ret.vld = -1;
    return ret;
}

inline mem_pump_data_t& mem_pump_data_t::operator=(const mem_pump_data_io_t& t)
{
    this->data = t(7, 0);
    this->vld = t(8, 8);
    return *this;
}

inline mem_pump_data_t& mem_pump_data_t::operator=(const mem_pump_data_sw_t& t)
{
    this->data = t.data;
    this->vld = t.vld;
    return *this;
}

inline ap_uint<16> mem_pump_data_sw_t::bits() const
{
    ap_uint<16> ret;
    ret(7, 0) = data;
    ret(15, 8) = vld;
    return ret;
}

inline mem_pump_data_sw_t& mem_pump_data_sw_t::operator=(const mem_pump_data_t& t)
{
    this->data = t.data;
    this->vld = t.vld;
    return *this;
}

inline mem_pump_data_sw_t& mem_pump_data_sw_t::operator=(const ap_uint<4096>& t)
{
    this->data = t(7, 0);
    this->vld = t(15, 8);
    return *this;
}

inline sorter_ctrl_out_t sorter_ctrl_out_t::operator~() const
{
    sorter_ctrl_out_t ret;
    ret.accept_input = ~(this->accept_input);
    ret.pump_start = ~(this->pump_start);
    ret.pump_done = ~(this->pump_done);
    return ret;
}

inline sorter_ctrl_out_t sorter_ctrl_out_t::operator&(const sorter_ctrl_out_t& t) const
{
    sorter_ctrl_out_t ret;
    ret.accept_input = (this->accept_input) & t.accept_input;
    ret.pump_start = (this->pump_start) & t.pump_start;
    ret.pump_done = (this->pump_done) & t.pump_done;
    return ret;
}

inline sorter_ctrl_out_t sorter_ctrl_out_t::operator|(const sorter_ctrl_out_t& t) const
{
    sorter_ctrl_out_t ret;
    ret.accept_input = (this->accept_input) | t.accept_input;
    ret.pump_start = (this->pump_start) | t.pump_start;
    ret.pump_done = (this->pump_done) | t.pump_done;
    return ret;
}

inline sorter_ctrl_out_t sorter_ctrl_out_t::operator^(const sorter_ctrl_out_t& t) const
{
    sorter_ctrl_out_t ret;
    ret.accept_input = (this->accept_input) ^ t.accept_input;
    ret.pump_start = (this->pump_start) ^ t.pump_start;
    ret.pump_done = (this->pump_done) ^ t.pump_done;
    return ret;
}

inline ap_uint<3> sorter_ctrl_out_t::bits() const
{
    ap_uint<3> ret;
    ret(0, 0) = accept_input;
    ret(1, 1) = pump_start;
    ret(2, 2) = pump_done;
    return ret;
}

inline sorter_ctrl_out_t sorter_ctrl_out_t::zeros()
{
    sorter_ctrl_out_t ret;
    ret.accept_input = 0;
    ret.pump_start = 0;
    ret.pump_done = 0;
    return ret;
}

inline sorter_ctrl_out_t sorter_ctrl_out_t::ones()
{
    sorter_ctrl_out_t ret;
    ret.accept_input = -1;
    ret.pump_start = -1;
    ret.pump_done = -1;
    return ret;
}

inline sorter_ctrl_out_t& sorter_ctrl_out_t::operator=(const sorter_ctrl_out_io_t& t)
{
    this->accept_input = t(0, 0);
    this->pump_start = t(1, 1);
    this->pump_done = t(2, 2);
    return *this;
}

inline sorter_ctrl_out_t& sorter_ctrl_out_t::operator=(const sorter_ctrl_out_sw_t& t)
{
    this->accept_input = t.accept_input;
    this->pump_start = t.pump_start;
    this->pump_done = t.pump_done;
    return *this;
}

inline ap_uint<24> sorter_ctrl_out_sw_t::bits() const
{
    ap_uint<24> ret;
    ret(7, 0) = accept_input;
    ret(15, 8) = pump_start;
    ret(23, 16) = pump_done;
    return ret;
}

inline sorter_ctrl_out_sw_t& sorter_ctrl_out_sw_t::operator=(const sorter_ctrl_out_t& t)
{
    this->accept_input = t.accept_input;
    this->pump_start = t.pump_start;
    this->pump_done = t.pump_done;
    return *this;
}

inline sorter_ctrl_out_sw_t& sorter_ctrl_out_sw_t::operator=(const ap_uint<4096>& t)
{
    this->accept_input = t(7, 0);
    this->pump_start = t(15, 8);
    this->pump_done = t(23, 16);
    return *this;
}

inline collector_t collector_t::operator~() const
{
    collector_t ret;
    ret.id = ~(this->id);
    ret.data = ~(this->data);
    ret.be = ~(this->be);
    ret.vld = ~(this->vld);
    return ret;
}

inline collector_t collector_t::operator&(const collector_t& t) const
{
    collector_t ret;
    ret.id = (this->id) & t.id;
    ret.data = (this->data) & t.data;
    ret.be = (this->be) & t.be;
    ret.vld = (this->vld) & t.vld;
    return ret;
}

inline collector_t collector_t::operator|(const collector_t& t) const
{
    collector_t ret;
    ret.id = (this->id) | t.id;
    ret.data = (this->data) | t.data;
    ret.be = (this->be) | t.be;
    ret.vld = (this->vld) | t.vld;
    return ret;
}

inline collector_t collector_t::operator^(const collector_t& t) const
{
    collector_t ret;
    ret.id = (this->id) ^ t.id;
    ret.data = (this->data) ^ t.data;
    ret.be = (this->be) ^ t.be;
    ret.vld = (this->vld) ^ t.vld;
    return ret;
}

inline ap_uint<137> collector_t::bits() const
{
    ap_uint<137> ret;
    ret(3, 0) = id;
    ret(131, 4) = data;
    ret(135, 132) = be;
    ret(136, 136) = vld;
    return ret;
}

inline collector_t collector_t::zeros()
{
    collector_t ret;
    ret.id = 0;
    ret.data = 0;
    ret.be = 0;
    ret.vld = 0;
    return ret;
}

inline collector_t collector_t::ones()
{
    collector_t ret;
    ret.id = -1;
    ret.data = -1;
    ret.be = -1;
    ret.vld = -1;
    return ret;
}

inline collector_t& collector_t::operator=(const collector_io_t& t)
{
    this->id = t(3, 0);
    this->data = t(131, 4);
    this->be = t(135, 132);
    this->vld = t(136, 136);
    return *this;
}

inline collector_t& collector_t::operator=(const collector_sw_t& t)
{
    this->id = t.id;
    this->data = t.data;
    this->be = t.be;
    this->vld = t.vld;
    return *this;
}

inline ap_uint<152> collector_sw_t::bits() const
{
    ap_uint<152> ret;
    ret(7, 0) = id;
    ret(135, 8) = data;
    ret(143, 136) = be;
    ret(151, 144) = vld;
    return ret;
}

inline collector_sw_t& collector_sw_t::operator=(const collector_t& t)
{
    this->id = t.id;
    this->data = t.data;
    this->be = t.be;
    this->vld = t.vld;
    return *this;
}

inline collector_sw_t& collector_sw_t::operator=(const ap_uint<4096>& t)
{
    this->id = t(7, 0);
    this->data = t(135, 8);
    this->be = t(143, 136);
    this->vld = t(151, 144);
    return *this;
}

inline main_ctrl_t main_ctrl_t::operator~() const
{
    main_ctrl_t ret;
    ret.reset_col_mux = ~(this->reset_col_mux);
    ret.pump_col_mux = ~(this->pump_col_mux);
    return ret;
}

inline main_ctrl_t main_ctrl_t::operator&(const main_ctrl_t& t) const
{
    main_ctrl_t ret;
    ret.reset_col_mux = (this->reset_col_mux) & t.reset_col_mux;
    ret.pump_col_mux = (this->pump_col_mux) & t.pump_col_mux;
    return ret;
}

inline main_ctrl_t main_ctrl_t::operator|(const main_ctrl_t& t) const
{
    main_ctrl_t ret;
    ret.reset_col_mux = (this->reset_col_mux) | t.reset_col_mux;
    ret.pump_col_mux = (this->pump_col_mux) | t.pump_col_mux;
    return ret;
}

inline main_ctrl_t main_ctrl_t::operator^(const main_ctrl_t& t) const
{
    main_ctrl_t ret;
    ret.reset_col_mux = (this->reset_col_mux) ^ t.reset_col_mux;
    ret.pump_col_mux = (this->pump_col_mux) ^ t.pump_col_mux;
    return ret;
}

inline ap_uint<2> main_ctrl_t::bits() const
{
    ap_uint<2> ret;
    ret(0, 0) = reset_col_mux;
    ret(1, 1) = pump_col_mux;
    return ret;
}

inline main_ctrl_t main_ctrl_t::zeros()
{
    main_ctrl_t ret;
    ret.reset_col_mux = 0;
    ret.pump_col_mux = 0;
    return ret;
}

inline main_ctrl_t main_ctrl_t::ones()
{
    main_ctrl_t ret;
    ret.reset_col_mux = -1;
    ret.pump_col_mux = -1;
    return ret;
}

inline main_ctrl_t& main_ctrl_t::operator=(const main_ctrl_io_t& t)
{
    this->reset_col_mux = t(0, 0);
    this->pump_col_mux = t(1, 1);
    return *this;
}

inline main_ctrl_t& main_ctrl_t::operator=(const main_ctrl_sw_t& t)
{
    this->reset_col_mux = t.reset_col_mux;
    this->pump_col_mux = t.pump_col_mux;
    return *this;
}

inline ap_uint<16> main_ctrl_sw_t::bits() const
{
    ap_uint<16> ret;
    ret(7, 0) = reset_col_mux;
    ret(15, 8) = pump_col_mux;
    return ret;
}

inline main_ctrl_sw_t& main_ctrl_sw_t::operator=(const main_ctrl_t& t)
{
    this->reset_col_mux = t.reset_col_mux;
    this->pump_col_mux = t.pump_col_mux;
    return *this;
}

inline main_ctrl_sw_t& main_ctrl_sw_t::operator=(const ap_uint<4096>& t)
{
    this->reset_col_mux = t(7, 0);
    this->pump_col_mux = t(15, 8);
    return *this;
}


