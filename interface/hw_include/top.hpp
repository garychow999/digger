#pragma once

#define AP_INT_MAX_W 16384

#include <int_type.hpp>

#include <common.hpp>
#include <dero.hpp>
#include <dero2.hpp>
#include <keccak.hpp>

// include the def from sw_include
#include <top_def.hpp>

