#!/home/pchan/anaconda3/bin/python
import os
import sys

upper = int(sys.argv[1])

def gen_hls_def(upper:int):
    lines = []
    lines.append('#pragma once')
    lines.append('#include <ap_int.h>')
    lines.append('')

    for i in range(1, upper+1):
        lines.append('typedef ap_int<{0}>   I{0};'.format(i))
        lines.append('typedef ap_uint<{0}>  U{0};'.format(i))

    lines.append('')
    lines = [l + '\n' for l in lines]

    with open('int_type.hpp', 'w+') as stream:
        stream.writelines(lines)


def gen_verilog_def(upper:int):
    lines = []
    lines.append('package int_type_package;')
    lines.append('')

    for i in range(1, upper + 1):
        lines.append('typedef logic        [{0:4}:0]    U{1};'.format(i-1,i))
        if i != 1:
            lines.append('typedef logic signed [{0:4}:0]    I{1};'.format(i - 1, i))

    lines.append('')
    lines.append('endpackage')

    lines = [l + '\n' for l in lines]
    with open('int_type_package.sv', 'w+') as stream:
        stream.writelines(lines)


# entry point
gen_hls_def(upper)
gen_verilog_def(upper)
