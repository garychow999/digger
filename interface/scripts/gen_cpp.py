import os
import sys
import shutil
import yaml
import math
import random
from subprocess import Popen, PIPE


# the new structure
#==============================================================================
all_type = dict()
api_list = []                           # structure to add to api
processed = []                          # track which file is processed
ver = []                                # version of this interface
num_struct = 1
id_map = dict()

# hold the c2h and h2c struct
c2h_map = dict()
h2c_map = dict()

def get_type_info(type_name:str):
    if not(type_name in all_type.keys()):
        return False
    else:
        return all_type[type_name]


def get_bytewidth(bitwidth):
    bytewidth = int(math.ceil(bitwidth/8.0))
    if bytewidth <= 1:
        return 1
    elif bytewidth <= 2:
        return 2
    elif bytewidth <= 4:
        return 4
    elif bytewidth <= 8:
        return 8
    else:
        # it has to be char array, so just the original width
        return bytewidth


def add_primitive(type_name:str, base_type, int_type, bitwidth):

    # check if we already have this name
    ret = get_type_info(type_name)
    if ret != False:
        raise Exception('add_primitive: {0} already exist, orignial type is {1}'
        .format(type_name, ret['type']))

    t = dict()
    t['base_type']                      = base_type
    t['int_type']                       = int_type
    t['type']                           = 'P'
    t['bitwidth']                       = bitwidth
    t['bytewidth']                      = get_bytewidth(bitwidth)
    t['alen']                           = 0
    all_type[type_name]                 = t


def add_array(type_name, base_type, int_type, alen):

    # check if we already have this name
    ret = get_type_info(type_name)
    if ret != False:
        raise Exception('add_array: {0} already exist, orignial type is {1}'.format(
        type_name, ret['type']))

    # check if base type exist
    ret = get_type_info(base_type)
    if ret == False:
        raise Exception('add_array {0}: base_type={1} not found'.format(
        type_name, base_type))

    t = dict()
    t['base_type']                      = base_type
    t['int_type']                       = int_type
    t['type']                           = 'A'
    t['bitwidth']                       = ret['bitwidth'] * alen
    t['bytewidth']                      = ret['bytewidth'] * alen
    t['alen']                           = alen
    all_type[type_name]                 = t


def add_struct(type_name, kv):

    # check if we already have this name
    ret = get_type_info(type_name)
    if ret != False:
        raise Exception('add_struct: {0} already exist, orignial type is {1}'.format(
        type_name, ret['type']))

    bitwidth                            = 0
    bytewidth                           = 0

    for sub_type_name, sub_type in kv.items():
        ret = get_type_info(sub_type)
        if ret == False:
            raise Exception('add_struct not found, sub_type={}', sub_type)
        else:
            bitwidth                    = bitwidth + ret['bitwidth']
            bytewidth                   = bytewidth + ret['bytewidth']

    t = dict()
    t['base_type']                      = ''
    t['type']                           = 'S'
    t['bitwidth']                       = bitwidth
    t['bytewidth']                      = bytewidth
    t['num_64']                         = int(math.ceil(bytewidth/8.0))
    t['alen']                           = 0
    global num_struct
    t['id']                             = num_struct
    id_map[num_struct]                  = type_name
    num_struct                          = num_struct + 1
    all_type[type_name]                 = t


def read_headers(filename:str, inpath):
    """
    add all the type etc, no code generation here
    """
    print('read_headers: processing file = {0}'.format(filename))

    # load the yaml
    with open('{0}/{1}.yaml'.format(inpath, filename), 'r') as f:
        y = yaml.load(f, Loader=yaml.FullLoader)
        f.close()
    processed.append(filename)
  
    # recursive read all the header
    if 'include' in y:
        for i in y['include']:
            # recursive
            if i not in processed:
                read_headers(i, inpath)


    # definition
    if 'def' in y:
        for k, v in y['def'].items():
            # evaluate it into the global space
            cmd = 'global {0}; {0} = {1}'.format(k, v)
            exec(cmd)

    # typedef
    if 'typedef' in y:
        for k, v in y['typedef'].items():
            if 'leng' in v:
                # TODO: right now only support U/I
                base_type               = '{0}{1}'.format(v['type'], v['leng'])
                bitwidth = int(eval(v['leng']))
                int_type                = '{0}{1}'.format(
                    v['type'],
                    int(get_bytewidth(bitwidth)*8)
                )
                add_primitive(k, base_type, int_type, bitwidth)

            if 'array' in v:
                size = int(eval(v['array']))
                int_type = '{0}_int_t'.format(v['type'][:-2])
                add_array(k, v['type'], int_type, size)

    # struct
    if 'struct' in y:
        for k, v in y['struct'].items():
            add_struct(k, v)

    # api
    if 'api' in y:
        for k in y['api']:
            api_list.append(k)


def output_to_file(lines, filename):

    lines = [l + '\n' for l in lines]
    with open(filename, 'w+') as stream:
        stream.writelines(lines)


def gen_def(filename:str, inpath, sw_path):

    # load the yaml
    with open('{0}/{1}.yaml'.format(inpath, filename), 'r') as f:
        y = yaml.load(f, Loader=yaml.FullLoader)
        f.close()
    processed.append(filename)

    # recursive gen_def
    if 'include' in y:
        for i in y['include']:
            # recursive
            if i not in processed:
                gen_def(i, inpath, sw_path)


    lines = []
    lines.append('#pragma once')
    lines.append('')

    # generate all the definition
    if 'def' in y:
        for k, v in y['def'].items():
            l = '#define {0:<32}{1}'.format(k, v)
            lines.append(l)
        lines.append('')

    output_to_file(lines, '{0}/{1}_def.hpp'.format(sw_path, filename))


def gen_sw_header(filename:str, inpath, sw_path, top_level=False):

    # load the yaml
    with open('{0}/{1}.yaml'.format(inpath, filename), 'r') as f:
        y = yaml.load(f, Loader=yaml.FullLoader)
        f.close()
    processed.append(filename)


    lines = []
    lines.append('#pragma once')
    lines.append('')

    if top_level:
        magic = (ver[0] << 24) | (ver[1] << 16) | (ver[2])
        lines.append('// magic')
        lines.append('#define API_MAGIC                       {0}'.format(hex(magic)))
        lines.append('')

    lines.append('#pragma pack(push, 1)')
    lines.append('')

    lines.append('#include <{}_def.hpp>'.format(filename))
    lines.append('')

    # recursive gen_sw_header
    lines.append('#include <cstdint>')
    lines.append('')
    if 'include' in y:
        for i in y['include']:
            lines.append('#include <{}_sw.hpp>'.format(i))
            # recursive
            if i not in processed:
                gen_sw_header(i, inpath, sw_path)
    lines.append('')



    # typedef
    if 'typedef' in y:

        for k, v in y['typedef'].items():

            # integer type
            if v['type'] == 'U' or v['type'] == 'I':
                leng = int(eval(v['leng']))

                # big integer convert to array
                if leng > 64:
                    bytewidth = get_bytewidth(leng)
                    lines.append('typedef char {}_sw_t[{}];'.format(k[:-2], bytewidth))
                else:
                    this_type = ''
                    if leng <= 8:
                        this_type = 'int8_t'
                    elif leng <= 16:
                        this_type = 'int16_t'
                    elif leng <= 32:
                        this_type = 'int32_t'
                    else:
                        this_type = 'int64_t'

                    # signed 
                    if v['type'] == 'U':
                        this_type = 'u{}'.format(this_type)

                    lines.append('typedef {} {}_sw_t;'.format(this_type, k[:-2]))
            
            elif 'array' in v.keys():

                alen = int(eval(v['array']))
                lines.append('typedef {}_sw_t {}_sw_t[{}];'.format(v['type'][:-2], k[:-2], v['array']))

        lines.append('')
        lines.append('')
        lines.append('')

    # struct
    if 'struct' in y:
        for k1, v1 in y['struct'].items():
            lines.append('struct {}_sw_t {{'.format(k1[:-2]))
            # lines.append('    static const int struct_id = {};'.format(get_type_info(k1)['id']))
            append_struct_id(lines, k1)
            # lines.append('    static const int num_64 = {};'.format(get_type_info(k1)['num_64']))
            for k2, v2 in v1.items():
                lines.append('\t{}_sw_t {};'.format(v2[:-2], k2))
            lines.append('};')
            lines.append('')

    # enum class
    if 'enum' in y:
        lines.append('// enum')
        lines.append('//=============================================================================')
        for k1, v1 in y['enum'].items():
            lines.append('enum class {0} : {1}'.format(k1, v1['type']))
            lines.append('{')
            for k2, v2 in v1['items'].items():
                lines.append('    {0} = {1},'.format(k2, v2))
            lines.append('};')
            lines.append('')

    lines.append('#pragma pack(pop)')
    lines.append('')

    output_to_file(lines, '{0}/{1}_sw.hpp'.format(sw_path, filename))


def gen_hls_struct_fw(lines, k1, v1):

    lines.append('struct {};'.format(k1))
    lines.append('struct {}_sw_t;'.format(k1[:-2]))
    lines.append('')

    ret = get_type_info(k1)

    lines.append('struct {0} {{'.format(k1))

    # lines.append('    static const int struct_id = {0};'.format(ret['id']))
    append_struct_id(lines, k1)
    lines.append('    static const int bitwidth = {0};'.format(ret['bitwidth']))
    # lines.append('    static const int num_64 = {0};'.format(ret['num_64']))
    lines.append('')

    for k2, v2 in v1.items():
        lines.append('    {0:<36}{1};'.format(v2, k2))

    lines.append('')
    lines.append('    {0} operator~() const;'.format(k1))
    lines.append('    {0} operator&(const {0}& t) const;'.format(k1))
    lines.append('    {0} operator|(const {0}& t) const;'.format(k1))
    lines.append('    {0} operator^(const {0}& t) const;'.format(k1))
    lines.append('')
    lines.append('    ap_uint<{0}> bits() const;'.format(ret['bitwidth']))
    lines.append('    static {0} zeros();'.format(k1))
    lines.append('    static {0} ones();'.format(k1))
    lines.append('')
    # io_t
    lines.append('    {}& operator=(const {}_io_t& t);'.format(k1, k1[:-2]))
    # lines.append('    operator {}_io_t() const;'.format(k1[:-2]))
    # lines.append('')
    # sw_t
    lines.append('    {}& operator=(const {}_sw_t& t);'.format(k1, k1[:-2]))
    # lines.append('    operator {}_sw_t() const;'.format(k1[:-2]))
    lines.append('};')
    lines.append('')

    # sw_t type
    lines.append('struct {0}_sw_t {{'.format(k1[:-2]))
    lines.append('')
    for k2, v2 in v1.items():
        lines.append('    {0}_sw_t {1};'.format(v2[:-2], k2))
    lines.append('')
    # lines.append('    ap_uint<{0}> bits() const;'.format(int(ret['num_64']*64)))
    lines.append('    ap_uint<{0}> bits() const;'.format(int(ret['bytewidth']*8)))
    lines.append('    {}_sw_t& operator=(const {}& t);'.format(k1[:-2], k1))
    # lines.append('    {0}_sw_t& operator=(const ap_uint<{1}>& t);'.format(k1[:-2], int(ret['num_64']*64)))
    lines.append('    {0}_sw_t& operator=(const ap_uint<4096>& t);'.format(k1[:-2]))
    lines.append('};')
    lines.append('')


def gen_hls_struct_impl(lines, k1, v1):

    # NOT
    lines.append('inline {0} {0}::operator~() const'.format(k1))
    lines.append('{')
    lines.append('    {0} ret;'.format(k1))
    for k2, v2 in v1.items():
        ret = get_type_info(v2)
        if ret['type']=='A':
            lines.append('    for(int i=0; i<{0}; i++)'.format(ret['alen']))
            lines.append('        ret.{0}[i] = ~(this->{0}[i]);'.format(k2))
        else:
            lines.append('    ret.{0} = ~(this->{0});'.format(k2))
    lines.append('    return ret;')
    lines.append('}')
    lines.append('')

    # AND
    lines.append('inline {0} {0}::operator&(const {0}& t) const'.format(k1))
    lines.append('{')
    lines.append('    {0} ret;'.format(k1))
    for k2, v2 in v1.items():
        ret = get_type_info(v2)
        if ret['type']=='A':
            lines.append('    for(int i=0; i<{0}; i++)'.format(ret['alen']))
            lines.append('        ret.{0}[i] = (this->{0}[i]) & (t.{0}[i]);'.format(k2))
        else:
            lines.append('    ret.{0} = (this->{0}) & t.{0};'.format(k2))
    lines.append('    return ret;')
    lines.append('}')
    lines.append('')

    # OR
    lines.append('inline {0} {0}::operator|(const {0}& t) const'.format(k1))
    lines.append('{')
    lines.append('    {0} ret;'.format(k1))
    for k2, v2 in v1.items():
        ret = get_type_info(v2)
        if ret['type']=='A':
            lines.append('    for(int i=0; i<{0}; i++)'.format(ret['alen']))
            lines.append('        ret.{0}[i] = (this->{0}[i]) | (t.{0}[i]);'.format(k2))
        else:
            lines.append('    ret.{0} = (this->{0}) | t.{0};'.format(k2))
    lines.append('    return ret;')
    lines.append('}')
    lines.append('')

    # XOR
    lines.append('inline {0} {0}::operator^(const {0}& t) const'.format(k1))
    lines.append('{')
    lines.append('    {0} ret;'.format(k1))
    for k2, v2 in v1.items():
        ret = get_type_info(v2)
        if ret['type']=='A':
            lines.append('    for(int i=0; i<{0}; i++)'.format(ret['alen']))
            lines.append('        ret.{0}[i] = (this->{0}[i]) ^ (t.{0}[i]);'.format(k2))
        else:
            lines.append('    ret.{0} = (this->{0}) ^ t.{0};'.format(k2))
    lines.append('    return ret;')
    lines.append('}')
    lines.append('')

    # bits()
    size = get_type_info(k1)['bitwidth']
    lines.append('inline ap_uint<{0}> {1}::bits() const'.format(size, k1))
    lines.append('{')
    lines.append('    ap_uint<{0}> ret;'.format(size))
    offset = 0
    for k2, v2 in v1.items():

        ret = get_type_info(v2)

        if ret['type'] == 'A':

            inner_ret = get_type_info(ret['base_type'])
            lines.append('    for(int i=0; i<{0}; i++)'.format(ret['alen']))

            if inner_ret['type'] == 'P':
                lines.append('        ret({1}+(i+1)*{2}-1,{1}+(i)*{2}) = {0}[i];'
                .format(k2, offset, inner_ret['bitwidth']))
            else:
                lines.append('        ret({1}+(i+1)*{2}-1,{1}+(i)*{2}) = {0}[i].bits();'
                .format(k2, offset, inner_ret['bitwidth']))

        elif ret['type'] == 'S':
            lines.append('    ret({1}, {2}) = {0}.bits();'
            .format(k2, offset + ret['bitwidth'] - 1, offset))

        else:
            lines.append('    ret({1}, {2}) = {0};'
            .format(k2, offset + ret['bitwidth'] - 1, offset))

        offset = offset + ret['bitwidth']

    lines.append('    return ret;')
    lines.append('}')
    lines.append('')

    # zeros
    lines.append('inline {0} {0}::zeros()'.format(k1))
    lines.append('{')
    lines.append('    {0} ret;'.format(k1))
    for k2, v2 in v1.items():
        ret = get_type_info(v2)
        if ret['type'] == 'A':
            lines.append('    for(int i=0; i<{0}; i++)'.format(ret['alen']))
            lines.append('        ret.{0}[i] = 0;'.format(k2))
        elif ret['type'] == 'S':
            lines.append('    ret.{0} = {1}::zeros();'.format(k2, v2))
        else:
            lines.append('    ret.{0} = 0;'.format(k2))
    lines.append('    return ret;')
    lines.append('}')
    lines.append('')

    # ones
    lines.append('inline {0} {0}::ones()'.format(k1))
    lines.append('{')
    lines.append('    {0} ret;'.format(k1))
    for k2, v2 in v1.items():
        ret = get_type_info(v2)
        if ret['type'] == 'A':
            lines.append('    for(int i=0; i<{0}; i++)'.format(ret['alen']))
            lines.append('        ret.{0}[i] = -1;'.format(k2))
        elif ret['type'] == 'S':
            lines.append('    ret.{0} = {1}::ones();'.format(k2, v2))
        else:
            lines.append('    ret.{0} = -1;'.format(k2))
    lines.append('    return ret;')
    lines.append('}')
    lines.append('')

    # convert from IO type
    size = get_type_info(k1)['bitwidth']
    lines.append('inline {0}& {0}::operator=(const {1}_io_t& t)'.format(k1, k1[:-2]))
    lines.append('{')
    offset = 0
    for k2, v2 in v1.items():
        ret = get_type_info(v2)
        if ret['type'] == 'A':
            inner_ret = get_type_info(ret['base_type'])
            lines.append('    for(int i=0; i<{0}; i++)'.format(ret['alen']))
            lines.append('        this->{0}[i] = t({1}+(i+1)*{2}-1,{1}+(i)*{2});'
            .format(k2, offset, inner_ret['bitwidth']))
        else:
            lines.append('    this->{0} = t({1}, {2});'.format(k2, offset+ret['bitwidth']-1, offset))
        offset = offset + ret['bitwidth']
    lines.append('    return *this;')
    lines.append('}')
    lines.append('')

    # # convert to IO type
    # size = get_type_info(k1)['bitwidth']
    # lines.append('inline {0}::operator {1}_io_t() const'.format(k1, k1[:-2]))
    # lines.append('{')
    # lines.append('    return bits();')
    # lines.append('}')
    # lines.append('')

    # convert from SW type
    size = get_type_info(k1)['bitwidth']
    lines.append('inline {0}& {0}::operator=(const {1}_sw_t& t)'.format(k1, k1[:-2]))
    lines.append('{')
    offset = 0
    for k2, v2 in v1.items():
        ret = get_type_info(v2)
        if ret['type'] == 'A':
            lines.append('    for(int i=0; i<{0}; i++)'.format(ret['alen']))
            lines.append('        this->{0}[i] = t.{0}[i];'.format(k2))
        else:
            # if ret['bitwidth'] > 64 and ret['type'] =='P':
            #     lines.append('    for(int i=0; i<{0}; i++)'.format(ret['bytewidth']))
            #     lines.append('        this->{0}((i+1)*8-1, i*8) = t.{0}[i];'.format(k2))
            lines.append('    this->{0} = t.{0};'.format(k2))
    lines.append('    return *this;')
    lines.append('}')
    lines.append('')

    # # convert to SW type
    # size = get_type_info(k1)['bitwidth']
    # lines.append('inline {0}::operator {1}_sw_t() const'.format(k1, k1[:-2]))
    # lines.append('{')
    # lines.append('    {}_sw_t ret;'.format(k1[:-2]))
    # for k2, v2 in v1.items():
    #     ret = get_type_info(v2)
    #     if ret['type'] == 'A':
    #         lines.append('    for(int i=0; i<{0}; i++)'.format(ret['alen']))
    #         lines.append('        ret.{0}[i] = this->{0}[i];'.format(k2))
    #     else:
    #         if ret['bitwidth'] > 64 and ret['type'] =='P':
    #             lines.append('    for(int i=0; i<{0}; i++)'.format(ret['bytewidth']))
    #             lines.append('        ret.{0}[i] = this->{0}((i+1)*8-1, i*8);'.format(k2))
    #         else:
    #             lines.append('    ret.{0} = this->{0};'.format(k2))

    # lines.append('    return ret;')
    # lines.append('}')
    # lines.append('')


    # # convert from IO type
    # size = get_type_info(k1)['bitwidth']
    # lines.append('inline {0}_int_t& {0}_int_t::operator=(const {1}& t)'.format(k1[:-2], k1))
    # lines.append('{')
    # offset = 0
    # for k2, v2 in v1.items():
    #     ret = get_type_info(v2)
    #     if ret['type'] == 'A':
    #         lines.append('    for(int i=0; i<{0}; i++)'.format(ret['alen']))
    #         lines.append('        this->{0}[i] = t.{0}[i];'.format(k2))
    #     else:
    #         lines.append('    this->{0} = t.{0};'.format(k2))

    # lines.append('    return *this;')
    # lines.append('}')
    # lines.append('')


    # sw bits()
    size = get_type_info(k1)['bytewidth'] * 8
    lines.append('inline ap_uint<{0}> {1}_sw_t::bits() const'.format(size, k1[:-2]))
    lines.append('{')
    lines.append('    ap_uint<{0}> ret;'.format(size))
    offset = 0
    for k2, v2 in v1.items():

        ret = get_type_info(v2)

        if ret['type'] == 'A':

            inner_ret = get_type_info(ret['base_type'])
            lines.append('    for(int i=0; i<{0}; i++)'.format(ret['alen']))

            if inner_ret['type'] == 'P':
                lines.append('        ret({1}+(i+1)*{2}-1,{1}+(i)*{2}) = {0}[i];'
                .format(k2, offset, inner_ret['bytewidth']*8))
            else:
                lines.append('        ret({1}+(i+1)*{2}-1,{1}+(i)*{2}) = {0}[i].bits();'
                .format(k2, offset, inner_ret['bytewidth']*8))

        elif ret['type'] == 'S':
            lines.append('    ret({1}, {2}) = {0}.bits();'
            .format(k2, offset + ret['bytewidth']*8 - 1, offset))

        else:
            lines.append('    ret({1}, {2}) = {0};'
            .format(k2, offset + ret['bytewidth']*8 - 1, offset))

        offset = offset + ret['bytewidth']*8

    lines.append('    return ret;')
    lines.append('}')
    lines.append('')

    # convert from _t to sw_t type
    size = get_type_info(k1)['bitwidth']
    lines.append('inline {1}_sw_t& {1}_sw_t::operator=(const {0}& t)'.format(k1, k1[:-2]))
    lines.append('{')
    offset = 0
    for k2, v2 in v1.items():
        ret = get_type_info(v2)
        if ret['type'] == 'A':
            lines.append('    for(int i=0; i<{0}; i++)'.format(ret['alen']))
            lines.append('        this->{0}[i] = t.{0}[i];'.format(k2))
        else:
            lines.append('    this->{0} = t.{0};'.format(k2))
    lines.append('    return *this;')
    lines.append('}')
    lines.append('')

    # convert from ap_uint to sw
    ret = get_type_info(k1)
    lines.append('inline {0}_sw_t& {0}_sw_t::operator=(const ap_uint<4096>& t)'
        .format(k1[:-2]))
    lines.append('{')
    offset = 0
    for k2, v2 in v1.items():
        ret = get_type_info(v2)
        if ret['type'] == 'A':
            inner_ret = get_type_info(ret['base_type'])
            lines.append('    for(int i=0; i<{0}; i++)'.format(ret['alen']))
            lines.append('        this->{0}[i] = t({1}+(i+1)*{2}-1,{1}+(i)*{2});'
            .format(k2, offset, inner_ret['bytewidth']*8))
        else:
            lines.append('    this->{0} = t({1}, {2});'.format(k2, offset+ret['bytewidth']*8-1, offset))
        offset = offset + ret['bytewidth'] * 8
    lines.append('    return *this;')
    lines.append('}')
    lines.append('')



############################################################################
def gen_hw_header(filename:str, inpath, hw_path, top_level=False):

    # load the yaml
    with open('{0}/{1}.yaml'.format(inpath, filename), 'r') as f:
        y = yaml.load(f, Loader=yaml.FullLoader)
        f.close()
    processed.append(filename)

    lines = []
    lines.append('#pragma once')
    lines.append('')

    if top_level:
        max_bitwidth = 1024
        for s in all_type.values():
            if s['type'] == 'S':
                max_bitwidth = max(max_bitwidth, s['num_64']*64)
        lines.append('#define AP_INT_MAX_W 16384')
        lines.append('')
        lines.append('#include <int_type.hpp>')
        lines.append('')

    # recursive gen_hw_header
    if 'include' in y:
        for i in y['include']:
            lines.append('#include <{}.hpp>'.format(i))
            # recursive
            if i not in processed:
                gen_hw_header(i, inpath, hw_path)

    lines.append('')
    lines.append('// include the def from sw_include')
    lines.append('#include <{}_def.hpp>'.format(filename))
    lines.append('')


    # typedef
    if 'typedef' in y:

        #HLS
        lines.append('// HLS typedef')
        lines.append('//=============================================================================')
        for k, v in y['typedef'].items():
            if 'array' in v:
                lines.append('typedef {0:<32}{1}[{2}];'.format(v['type'], k, v['array']))
            else:
                leng = eval(v['leng'])
                nptype = '{0}{1}'.format(v['type'], leng)
                lines.append('typedef {0:<32}{1};'.format(nptype, k))
        lines.append('//=============================================================================')
        lines.append('')


        # int type
        lines.append('// sw_t typedef')
        lines.append('//=============================================================================')
        for k, v in y['typedef'].items():
            if 'array' in v:
                lines.append('typedef {0}_sw_t {1}_sw_t [{2}];'.format(v['type'][:-2], k[:-2], v['array']))
            else:
                leng = eval(v['leng'])
                leng = int(get_bytewidth(leng) * 8)
                nptype = '{0}{1}'.format(v['type'], leng)
                lines.append('typedef {0} {1}_sw_t;'.format(nptype, k[:-2]))
        lines.append('//=============================================================================')
        lines.append('')

    # enum class
    if 'enum' in y:
        lines.append('// enum')
        lines.append('//=============================================================================')
        for k1, v1 in y['enum'].items():
            lines.append('enum class {0} : {1}'.format(k1, v1['type']))
            lines.append('{')
            for k2, v2 in v1['items'].items():
                lines.append('    {0} = {1},'.format(k2, v2))
            lines.append('};')
            lines.append('')

    # io type
    if 'struct' in y:

        lines.append('// IO type')
        lines.append('//=============================================================================')
        for name in y['struct'].keys():
            lines.append('typedef ap_uint<{}> {}_io_t;'.format(all_type[name]['bitwidth'], name[:-2]))
        lines.append('//=============================================================================')
        lines.append('')

    # structure forward declaration
    if 'struct' in y:
        lines.append('// HLS struct forward declaration')
        lines.append('//=============================================================================')
        for k1, v1 in y['struct'].items():
            gen_hls_struct_fw(lines, k1, v1)
        lines.append('//=============================================================================')
        lines.append('')

    # structure impl
    if 'struct' in y:
        lines.append('// HLS struct impl')
        lines.append('//=============================================================================')
        for k1, v1 in y['struct'].items():
            gen_hls_struct_impl(lines, k1, v1)
        lines.append('')

    output_to_file(lines, '{0}/{1}.hpp'.format(hw_path, filename))

    if top_level:
        gen_api(hw_path)


def gen_api(hw_path:str):

    # start writing the file
    lines = []
    lines.append('#pragma once')
    lines.append('')

    lines.append('#include <top.hpp>')
    lines.append('')

    lines.append('struct h2c_collection_t{')
    for k in h2c_map:
        lines.append("\t{0} {1};".format(k, k[:-2]))
    lines.append('};')

    output_to_file(lines, '{0}/api.hpp'.format(hw_path))

    # generate the
    lines = []
    for k in h2c_map:
        lines.append("\tn.h2c_collection.{0} = full_buffer;".format(k[:-2]))
        lines.append("\tn.h2c_collection.{0}.vld = n.h2c_collection.{0}.vld==1 && got_last && c.id=={1}::h2c_id ? 1 : 0;".format(k[:-2], k))
        lines.append("")
    output_to_file(lines, '{0}/h2c_gen_include.hpp'.format(hw_path))



def gen_api_verilog(sv_path:str):
    lines = []
    lines.append('`ifndef api_package')
    lines.append('`define api_package')

    lines.append('')

    lines.append('`include "top_package.sv"')
    lines.append('')

    # magic
    magic = (ver[0] << 24) | (ver[1] << 16) | (ver[2])
    lines.append('// magic')
    lines.append('`define API_MAGIC                       32\'h{0}'.format(hex(magic)))
    lines.append('')

    # the combined type
    lines.append('// the combined type')
    lines.append('typedef struct packed {')
    for t in reversed(api_list):
        l = '    {0}'.format(t).ljust(40)
        l = '{0}{1};'.format(l, t[0:-2])
        lines.append(l)
    lines.append('} all_t;')
    lines.append('')

    # postfix
    lines.append('`endif')

    output_to_file(lines, '{0}/api_package.sv'.format(sv_path))


def gen_verilog_struct(lines, k1, v1):
    lines.append('typedef struct packed {')
    a = []
    for k2, v2 in v1.items():
        ret = get_type_info(v2)
        a.append('    {0:<36}{1};'.format(v2, k2))
    for i in reversed(a):
        lines.append(i)
    lines.append('}} {0};'.format(k1))
    lines.append('')


def gen_verilog(filename:str, inpath, sv_path, top_level=False):

    print('gen_verilog: processing file = {0}'.format(filename))
    lines = []

    # load the yaml
    with open('{0}/{1}.yaml'.format(inpath, filename), 'r') as f:
        y = yaml.load(f, Loader=yaml.FullLoader)
        f.close()
    processed.append(filename)

    # ifdef guard
    if 'guard' in y:
        lines.append('`ifdef {0}'.format(filename.upper()))
        lines.append('')

    lines.append('`ifndef {0}_PACKAGE'.format(filename.upper()))
    lines.append('`define {0}_PACKAGE'.format(filename.upper()))
    lines.append('')

    # recursive read all the header
    if 'include' in y:
        for i in y['include']:
            lines.append('`include "{0}_package.sv"'.format(i))
            # recursive
            if i not in processed:
                gen_verilog(i, inpath, sv_path, False)
    lines.append('')

    # definition
    if 'def' in y:
        for k, v in y['def'].items():
            s = int(eval(k))
            l = '`define {0:<32}{1}'.format(k, s)
            lines.append(l)
        lines.append('')


    # typedef
    if 'typedef' in y:
        lines.append('// typedef')
        lines.append('//=============================================================================')
        for k, v in y['typedef'].items():
            if 'array' in v:
                array_size = get_type_info(k)['alen']
                lines.append('typedef {0}[{1}:0]{2};'.format(v['type'], array_size-1, k))
            else:
                leng = get_type_info(k)['bitwidth']
                nptype = 'logic[{0}:0]'.format(leng-1)
                lines.append('typedef {0:<32}{1};'.format(nptype, k))
    lines.append('')

    # structure
    if 'struct' in y:
        lines.append('// struct')
        lines.append('//=============================================================================')
        for k1, v1 in y['struct'].items():
            gen_verilog_struct(lines, k1, v1)
        lines.append('')

    # save it to file
    lines.append('')
    lines.append('`endif')
    # ifdef guard
    if 'guard' in y:
        lines.append('`endif')

    output_to_file(lines, '{0}/{1}_package.sv'.format(sv_path, filename))

    if top_level:
        gen_api_verilog(sv_path)


def gen_api_tpp(hw_path):
    lines = []

    for item in api_list:
        lines.append('ap_uint<{0}_t::num_64*64> {0}_io;'.format(item[:-2]))
        lines.append('{0}_io = all_data;'.format(item[:-2]))
        lines.append('{0}_sw_t {0}_sw;'.format(item[:-2]))
        lines.append('{0}_sw = {0}_io;'.format(item[:-2]))
        lines.append('{0}_t {0};'.format(item[:-2]))
        lines.append('{0} = {0}_sw;'.format(item[:-2]))
        lines.append('{0}.vld = (should_output && current_type == {0}_t::struct_id) ? 1 : 0;'
            .format(item[:-2]))
        lines.append('int_all.{0} = {0};'.format(item[:-2]))
        lines.append('')

    output_to_file(lines, '{0}/api_gen_cls.tpp'.format(hw_path))


def get_version():
    """
    Get the version
    """

    # make sure all the tags are fetch from the git
    ret3 = Popen('git fetch --tags', shell=True, stdout=PIPE).communicate()[0].decode("utf-8")

    # get the major and minor version from version txt
    with open('version', 'r') as stream:
        line = stream.readline()
        stream.close()

    t1 = line.split('.')
    ver.clear()
    ver.append(int(t1[0]))
    ver.append(int(t1[1]))

    # get incremental version from git
    cmd = 'git tag --list \'VER_{0:02}_{1:02}*\''.format(ver[0], ver[1])
    last_version_tag = Popen(cmd, shell=True, stdout=PIPE).communicate()[0].decode("utf-8").splitlines()

    if last_version_tag == []:
        ver.append(1)
    else:
        t1 = int(last_version_tag[-1].split('_')[-1])
        ver.append(int(t1)+1)

    print('next version is VER_{0:02}_{1:02}_{2:04}'.format(
        ver[0],  ver[1], ver[2]
    ))


# generate the c2h and h2c map
def get_id(inpath):

    with open('{0}/api.yaml'.format(inpath), 'r') as f:
        y = yaml.load(f, Loader=yaml.FullLoader)
        f.close()

    if 'c2h' in y:
        for item in y['c2h']:
            id = len(c2h_map)
            c2h_map[item] = id

    if 'h2c' in y:
        for item in y['h2c']:
            id = len(h2c_map)
            h2c_map[item] = id


def append_struct_id(lines, struct_name):

    if struct_name in c2h_map:
        lines.append('    static const int c2h_id = {};'
            .format(c2h_map[struct_name]))

    if struct_name in h2c_map:
        lines.append('    static const int h2c_id = {};'
            .format(h2c_map[struct_name]))


top = sys.argv[1]
in_path = sys.argv[2]
sw_path = sys.argv[3]
hw_path = sys.argv[4]
sv_path = sys.argv[5]

# # actually calling
get_version()

#get_id(in_path)

processed.clear()
read_headers(top, in_path)


processed.clear()
gen_def(top, in_path, sw_path)


processed.clear()
gen_sw_header(top, in_path, sw_path, True)


processed.clear()
gen_hw_header(top, in_path, hw_path, True)


processed.clear()
gen_verilog(top, in_path, sv_path, True)
