import os
import sys
import shutil
import yaml
import math
import random
from subprocess import Popen, PIPE

ver = []                                # version of this interface

def get_version():
    """
    Get the version
    """

    # make sure all the tags are fetch from the git
    ret3 = Popen('git fetch --tags', shell=True, stdout=PIPE).communicate()[0].decode("utf-8")

    # get the major and minor version from version txt
    with open('version', 'r') as stream:
        line = stream.readline()
        stream.close()

    t1 = line.split('.')
    ver.clear()
    ver.append(int(t1[0]))
    ver.append(int(t1[1]))

    # get incremental version from git
    cmd = 'git tag --list \'VER_{0:02}_{1:02}*\''.format(ver[0], ver[1])
    last_version_tag = Popen(cmd, shell=True, stdout=PIPE).communicate()[0].decode("utf-8").splitlines()

    if last_version_tag == []:
        ver.append(1)
    else:
        t1 = int(last_version_tag[-1].split('_')[-1])
        ver.append(int(t1)+1)

    print('next version is VER_{0:02}_{1:02}_{2:04}'.format(
        ver[0],  ver[1], ver[2]
    ))


def dist():
    ret1 = Popen('git status --porcelain', shell=True, stdout=PIPE).communicate()[0].decode("utf-8")
    if ret1 != '':
        exit('!!!! un-committed changes, please try again')

    # set the tag id
    tag_id = 'VER_{0:02}_{1:02}_{2:04}'.format(ver[0], ver[1], ver[2])
    print('tag_id : {0}'.format(tag_id))

    # tag the version
    cmd = 'git tag -a {0} -m "{0}"; git push origin {0}; git push'.format(tag_id)
    Popen(cmd, shell=True, stdout=PIPE).wait()


get_version()
dist()

