`include "dero_package.sv"

module tb();

logic clk;
mem_cmd_t cmd;
mem_data_t dout;
mem_pump_data_t pump_dout;

mem dut(
    .clk(clk),
    .cmd(cmd),
    .dout(dout),
    .pump_dout(pump_dout)
);

initial begin
    clk = 0;
    cmd = 0;

    #1000;

    // first do a write both
    // without vld
    cmd.wcmd.addr = 999;
    cmd.wcmd.data.key = 111;
    cmd.wcmd.data.data = 11;
    cmd.wcmd.both = 1;
    cmd.wcmd.vld = 0;
    #100;
    cmd = 0;
    #1000;

    // read
    // will return vld in data, but data = 0
    cmd.rcmd.addr = 999;
    cmd.rcmd.id = 10;
    cmd.rcmd.init_addrs = 123;
    cmd.rcmd.pump = 0;
    cmd.rcmd.vld = 1;
    #100;
    cmd = 0;
    #1000;


    // first do a write both
    // with vld
    cmd.wcmd.addr = 999;
    cmd.wcmd.data.key = 111;
    cmd.wcmd.data.data = 11;
    cmd.wcmd.both = 1;
    cmd.wcmd.vld = 1;
    #100;
    cmd = 0;
    #1000;

    // read
    // data vld, data = 111, 11
    cmd.rcmd.addr = 999;
    cmd.rcmd.id = 10;
    cmd.rcmd.init_addrs = 123;
    cmd.rcmd.pump = 0;
    cmd.rcmd.vld = 1;
    #100;
    cmd = 0;
    #1000;

    // read pump
    cmd.rcmd.addr = 999;
    cmd.rcmd.id = 10;
    cmd.rcmd.init_addrs = 123;
    cmd.rcmd.pump = 1;
    cmd.rcmd.vld = 1;
    #100;
    cmd = 0;
    #1000;


    // write only the upper part, with 222
    // with vld
    cmd.wcmd.addr = 999;
    cmd.wcmd.data.key = 222;
    cmd.wcmd.data.data = 22;
    cmd.wcmd.both = 0;
    cmd.wcmd.vld = 1;
    #100;
    cmd = 0;
    #1000;


    // read again
    // data vld, data = 222, 22
    cmd.rcmd.addr = 999;
    cmd.rcmd.id = 10;
    cmd.rcmd.init_addrs = 123;
    cmd.rcmd.pump = 0;
    cmd.rcmd.vld = 1;
    #100;
    cmd = 0;
    #1000;


    // read pump
    // data = 11
    cmd.rcmd.addr = 999;
    cmd.rcmd.id = 10;
    cmd.rcmd.init_addrs = 123;
    cmd.rcmd.pump = 1;
    cmd.rcmd.vld = 1;
    #100;
    cmd = 0;
    #1000;

    $finish;
end

always #50 clk = ~clk;

endmodule