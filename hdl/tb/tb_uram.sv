module tb();

 logic clk;
 logic [11:0] waddr;
 logic [71:0] din;
 logic [8:0] din_be;
 logic [11:0] raddr;
 wire [71:0] dout;

uram dut(
    .clk(clk),
    .waddr(waddr),
    .din(din),
    .din_be(din_be),
    .raddr(raddr),
    .dout(dout)
);

initial begin
    clk = 0;
    waddr = 0;
    din = 0;
    din_be = 0;
    raddr = 0;

    #500;

    // write a data
    waddr = 1;
    din = -1;
    din_be = 9'b1_1111_1110;
    #500;

    // read
    raddr = 1;
    #500;


    $finish;
end

always #50 clk = ~clk;

endmodule