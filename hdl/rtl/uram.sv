// N.B.: the read latency is now 3

module uram (
    input clk,
    input [11:0] waddr,
    input [71:0] din,
    input [8:0] din_be,
    input [11:0] raddr,
    output [71:0] dout
);

// A: read, B: write
URAM288_BASE #(
.AUTO_SLEEP_LATENCY(8), // Latency requirement to enter sleep mode
.AVG_CONS_INACTIVE_CYCLES(10), // Average concecutive inactive cycles when is SLEEP mode for power
// estimation
.BWE_MODE_A("PARITY_INDEPENDENT"), // Port A Byte write control
.BWE_MODE_B("PARITY_INDEPENDENT"), // Port B Byte write control
.EN_AUTO_SLEEP_MODE("FALSE"), // Enable to automatically enter sleep mode
.EN_ECC_RD_A("FALSE"), // Port A ECC encoder
.EN_ECC_RD_B("FALSE"), // Port B ECC encoder
.EN_ECC_WR_A("FALSE"), // Port A ECC decoder
.EN_ECC_WR_B("FALSE"), // Port B ECC decoder
.IREG_PRE_A("TRUE"), // Optional Port A input pipeline registers
.IREG_PRE_B("TRUE"), // Optional Port B input pipeline registers
.IS_CLK_INVERTED(1'b0), // Optional inverter for CLK
.IS_EN_A_INVERTED(1'b0), // Optional inverter for Port A enable
.IS_EN_B_INVERTED(1'b0), // Optional inverter for Port B enable
.IS_RDB_WR_A_INVERTED(1'b0), // Optional inverter for Port A read/write select
.IS_RDB_WR_B_INVERTED(1'b0), // Optional inverter for Port B read/write select
.IS_RST_A_INVERTED(1'b0), // Optional inverter for Port A reset
.IS_RST_B_INVERTED(1'b0), // Optional inverter for Port B reset
.OREG_A("TRUE"), // Optional Port A output pipeline registers
.OREG_B("TRUE"), // Optional Port B output pipeline registers
.OREG_ECC_A("FALSE"), // Port A ECC decoder output
.OREG_ECC_B("FALSE"), // Port B output ECC decoder
.RST_MODE_A("SYNC"), // Port A reset mode
.RST_MODE_B("SYNC"), // Port B reset mode
.USE_EXT_CE_A("FALSE"), // Enable Port A external CE inputs for output registers
.USE_EXT_CE_B("FALSE") // Enable Port B external CE inputs for output registers
)
URAM288_BASE_inst (
.DBITERR_A(), // 1-bit output: Port A double-bit error flag status
.DBITERR_B(), // 1-bit output: Port B double-bit error flag status
.DOUT_A(dout), // 72-bit output: Port A read data output
.DOUT_B(), // 72-bit output: Port B read data output
.SBITERR_A(), // 1-bit output: Port A single-bit error flag status
.SBITERR_B(), // 1-bit output: Port B single-bit error flag status
.ADDR_A({11'b0, raddr}), // 23-bit input: Port A address
.ADDR_B({11'b0, waddr}), // 23-bit input: Port B address
.BWE_A(9'b0), // 9-bit input: Port A Byte-write enable
.BWE_B(din_be), // 9-bit input: Port B Byte-write enable
.CLK(clk), // 1-bit input: Clock source
.DIN_A(72'b0), // 72-bit input: Port A write data input
.DIN_B(din), // 72-bit input: Port B write data input
.EN_A(1'b1), // 1-bit input: Port A enable
.EN_B(1'b1), // 1-bit input: Port B enable
.INJECT_DBITERR_A(1'b0), // 1-bit input: Port A double-bit error injection
.INJECT_DBITERR_B(1'b0), // 1-bit input: Port B double-bit error injection
.INJECT_SBITERR_A(1'b0), // 1-bit input: Port A single-bit error injection
.INJECT_SBITERR_B(1'b0), // 1-bit input: Port B single-bit error injection
.OREG_CE_A(1'b1), // 1-bit input: Port A output register clock enable
.OREG_CE_B(1'b1), // 1-bit input: Port B output register clock enable
.OREG_ECC_CE_A(1'b1), // 1-bit input: Port A ECC decoder output register clock enable
.OREG_ECC_CE_B(1'b1), // 1-bit input: Port B ECC decoder output register clock enable
.RDB_WR_A(1'b0), // 1-bit input: Port A read/write select
.RDB_WR_B(1'b1), // 1-bit input: Port B read/write select
.RST_A(1'b0), // 1-bit input: Port A asynchronous or synchronous reset for output
// registers
.RST_B(1'b0), // 1-bit input: Port B asynchronous or synchronous reset for output
// registers
.SLEEP(1'b0) // 1-bit input: Dynamic power gating control
);
    
endmodule