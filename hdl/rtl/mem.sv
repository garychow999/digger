// N.B.:
// [71:8] => data.key
// [7:0] => data.data

// TODO:
// 1. think about the read / write pipeline latency, will it mess up operations

`include "dero_package.sv"

module mem (
    input clk,
    input mem_cmd_t cmd,
    output mem_data_t dout,
    output mem_pump_data_t pump_dout
);

// connection to the uram
logic [11:0] waddr;
logic [71:0] din;
logic [8:0] din_be;
logic [11:0] raddr;
logic [71:0] uram_dout;

uram uram_inst(
    .clk(clk),
    .waddr(waddr),
    .din(din),
    .din_be(din_be),
    .raddr(raddr),
    .dout(uram_dout)
);

// write
always_ff @( posedge clk ) begin : write_pipeline
    waddr <= cmd.wcmd.addr;
    din[71:8] <= 
        {0, cmd.wcmd.data.key, cmd.wcmd.data.data};
    din[7:0] <= cmd.wcmd.data.data;
    if (cmd.wcmd.vld) begin
        din_be[8:1] <= -1;
        din_be[0:0] <= cmd.wcmd.both;
    end else begin
        din_be <= 0;
    end
end

// read
always_comb begin : read_comb
    raddr = cmd.rcmd.addr;
end

mem_rcmd_t rcmd1, rcmd2, rcmd3;
always_ff @( posedge clk ) begin : read_pipeline
    rcmd1 <= cmd.rcmd;
    rcmd2 <= rcmd1;
    rcmd3 <= rcmd2;

    // normal path
    dout.id <= rcmd3.id;
    dout.data.key <= uram_dout[71:16];
    dout.data.data <= uram_dout[15:8];
    dout.init_addrs <= rcmd3.init_addrs;
    dout.vld <= rcmd3.vld & ~rcmd3.pump;

    // pump data path
    pump_dout.data <= uram_dout[7:0];
    pump_dout.vld <= rcmd3.vld & rcmd3.pump;
end
    
endmodule