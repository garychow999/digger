// mux the output of a set of collectors to one single output
// using just 'OR'
// manually pipelined for good frequency

`include "dero_package.sv"

module col_mux (
    input clk,
    input collector_t din[0:COLLECTOR_GROUP-1],
    output collector_t dout
);

collector_t int_din[0:COLLECTOR_GROUP-1];
collector_t int_dout;

integer i;

always_ff @( posedge clk ) begin : mainblock
    for (i=0; i<COLLECTOR_GROUP; i++)
    begin
        int_din[i] <= din[i];
        dout <= int_dout;
    end
end

always_comb begin : or_block
    int_dout = din[0];
    for (i=1; i<COLLECTOR_GROUP; i++) int_dout = int_dout | dout[i];
end

endmodule