// this is the memory module used for simulation
// for now it does not exactly replicate the hdl
// module, and we cannot use the blackbox method
// as blackbox does not support struct

#include <top.hpp>

class Mem {

    static const int P = 3;
    mem_cmd_t pcmd[P];

    // the 2 memory
    sorter_int_key_data_t main_mem[1 << SORTER_AW];
    sorter_data_t pump_mem[1 << SORTER_AW];

    Mem() {
        for (int i=0; i<P; i++)
            pcmd[i] = mem_cmd_t::zeros();
    }

void process_once(
    mem_cmd_t cmd,
    mem_data_t& dout,
    mem_pump_data_t& pump_dout
){
    // read main
    auto rcmd = pcmd[P-1].rcmd;
    dout.id = rcmd.id;
    dout.data = main_mem[rcmd.addr];
    dout.init_addrs = rcmd.init_addrs;
    dout.vld = rcmd.vld & (~rcmd.pump);

    // read pump
    pump_dout.data = pump_mem[rcmd.addr];
    pump_dout.pump_start = rcmd.pump_start;
    pump_dout.pump_end = rcmd.pump_end;
    pump_dout.vld = rcmd.vld & rcmd.pump;

    // write
    auto wcmd = pcmd[P-1].wcmd;
    if (wcmd.vld) {
        main_mem[wcmd.addr] = wcmd.data;
        if (wcmd.both)
            pump_mem[wcmd.addr] = wcmd.data.data;
    }

    // pipeline the command
    for (int i=P-1; i>=1; i--)
        pcmd[i] = pcmd[i-1];
    // input
    pcmd[0] = cmd;
}
};