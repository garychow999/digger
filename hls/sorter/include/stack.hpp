#pragma once
#include <top.hpp>

template <class T, const int AW=6>
class Stack {
public:

    static const int NUM = 1<<AW;
    using ptr_t = ap_uint<AW>;
    T data[NUM];

    struct State {
        ptr_t tail, tail_m1;
        T pdata_out;
        vld_t cnt;
    };

    Stack() {
        #pragma HLS BIND_STORAGE variable=data type=ram_s2p impl=lutram latency=1
        #pragma HLS aggregate variable=data compact=bit
    }

    State c;

bool has_data() {
    return (c.tail != 0 && c.cnt == 0);
}

T top() {
    return c.pdata_out;
}

// TODO: gurad read
void process_once(
    vld_t reset,
    bool ext_read,
    bool write,
    T din
) {
    State n = c;

    // guard the read by actually has data
    bool read = ext_read && has_data();

    // read for next cycle
    n.pdata_out = data[c.tail_m1];

    // write
    if (read && write) {
        data[c.tail_m1] = din;
    } else if (write) {
        data[c.tail] = din;
    }

    // move the tail
    if (read && !write) {
        n.tail = c.tail - 1;
        n.tail_m1 = c.tail_m1 - 1;
    } else if (!read && write) {
        n.tail = c.tail + 1;
        n.tail_m1 = c.tail_m1 + 1;
    }

    // after each read or write invalid the output for 1 cycle
    if (read || write) {
        n.cnt = 1;
    } else {
        n.cnt = 0;
    }

    if (reset) {
        n.tail = 0;
        n.tail_m1 = -1;
        n.cnt = 1;
    }
    c = n;
}
};