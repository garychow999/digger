/* READ timing: (sorting stage)
 *  --------------------------------------------------------------------------------------------------------
 *  | rcmd_state | get_stack | sorting  | sorting  | sorting  | sorting  | sorting  | sorting  | get_stack |
 *  | rbounds    | -         | 0_0, 0_2 | 0_0, 0_2 | 0_0, 0_2 | 1_0, 1_2 | 1_0, 1_2 | 1_0, 1_2 | -         |
 *  | raddrs     | -         | 0_0, 0_2 | 0_1, 0_2 | 0_1, 0_1 | 1_0, 1_2 | 1_1, 1_2 | 1_1, 1_1 | -         |
 *  | rid        | 0         | 1        | 1        | 1        | 2        | 2        | 2        | 2         |
 *  --------------------------------------------------------------------------------------------------------
 *  | rcmd (out) |                                                                                         |
 *  --------------------------------------------------------------------------------------------------------
 *  | addr       | -         | 0_0      | 0_2      | 0_1      | 1_0      | 1_2      | 1_1      | -         |
 *  | init_addrs |           | 0_0, 0_2 | 0_0, 0_2 | 0_0, 0_2 | 1_0, 1_2 | 1_0, 1_2 | 1_0, 1_2 | -         |
 *  | id         | id        | 1        | 1        | 1        | 2        | 2        | 2        | 2         |
 *  | pump       | -         | 0        | 0        | 0        | 0        | 0        | 0        | -         |
 *  | vld        | 0         | 1        | 1        | 1        | 1        | 1        | 1        | 0         |
 *  --------------------------------------------------------------------------------------------------------
 */

/* READ timing: (pump_output stage)
 *  -------------------------------------------------------------------------
 *  | rcmd_state | get_stack_pump | pump_output | pump_output | pump_output |
 *  | pump_head  | -              | 0_0         | 0_1         | 0_2         |
 *  | pump_tail  | -              | 0_2         | 0_2         | 0_2         |
 *  -------------------------------------------------------------------------
 *  | rcmd (out) |                                                          |
 *  -------------------------------------------------------------------------
 *  | addr       | -              | 0_0         | 0_1         | 0_2         |
 *  | init_addrs | -              | -           | -           | -           |
 *  | id         | -              | -           | -           | -           |
 *  | pump       | -              | 1           | 1           | 1           |
 *  | vld        | 0              | 1           | 1           | 1           |
 *  -------------------------------------------------------------------------
 */

#pragma once
#include <top.hpp>

#include <stack.hpp>

// template<const int PART_AW=3>
class Sorter {
public:

    static const int PART_AW = 3;
    static const int PART_NUM = 1<<PART_AW;

    using num_t = ap_uint<PART_AW>;
    using finish_t = ap_uint<4>;

    enum class WCMD : unsigned {
        get_input,
        wait_pump_finish,
        write_stack,
        get_median,
        sorting,
    };
    enum class RCMD : unsigned {
        wait_input_finish,
        get_stack,
        sorting,
        get_stack_pump,
        pump_output,
    };

    struct State {
        // cmd state
        WCMD wcmd_state;
        RCMD rcmd_state;
        // pipeine the decision to use grow on lower side
        bool use_lower[PART_NUM];
        // the pointer
        sorter_addr_t head[PART_NUM];
        sorter_addr_t tail[PART_NUM];
        // rotate cnt and is_write_pump
        num_t rot_cnt;
        // read
        mem_id_t rid;
        bool rprior;
        stack_t rbounds, raddrs;
        // write
        sorter_int_key_data_t median;
        mem_id_t wid;
        stack_t wbounds, waddrs;
        // the finish cnt
        finish_t finish_cnt;
        // pump_head and pump_tail
        sorter_addr_t pump_head, pump_tail;
        // ub_stack
        bool ub_stack_vld;
        stack_t ub_stack;
        // indicated where does we write
        bool write_lower, write_lower_vld;
        mem_id_t write_lower_id;
        // total number of pairs
        sorter_addr_t num_pair;

        State() {
            #pragma hls array_partition variable=head
            #pragma hls array_partition variable=tail
        }
    };

    State c;
    Stack<stack_t, 6> stack;
    Stack<stack_t, 5> pump_stack;

void process_once(
    vld_t reset,
    sorter_key_data_t din,
    vld_t din_done,
    mem_data_t mdata,
    mem_cmd_t& mcmd,
    sorter_ctrl_out_t& ctrl_out
) {
    auto n = c;

    // use_lower
    // space[0] (head/tail0) space[1]
    sorter_addr_t space[PART_NUM];
    for (int i=0; i<PART_NUM; i++)
        space[i] = c.head[i] - c.tail[(i-1)%PART_NUM];
    for (int i=0; i<PART_NUM; i++)
        n.use_lower[i] = space[i] >= space[(i+1)%PART_NUM];

    // head/tail pointer used in input stage
    static const int TB = SORTER_INT_KEY_WIDTH;
    sorter_int_key_t key = din.key;
    ap_uint<PART_AW> pb = key(TB-1, TB-1-PART_AW);
    for (int i=0; i<PART_NUM; i++) {
        if (reset==1 || (c.wcmd_state==WCMD::get_median &&
            c.rcmd_state == RCMD::get_stack_pump))
        {
            sorter_addr_t addr;
            addr = (ap_uint<PART_AW>(i), ap_uint<SORTER_AW-PART_AW>(0));
            n.head[i] = n.tail[i] = addr;
        } else if (c.wcmd_state == WCMD::write_stack)
        {
            n.head[i] = c.head[(i+1)%PART_NUM];
            n.tail[i] = c.tail[(i+1)%PART_NUM];
        } else if ((c.wcmd_state == WCMD::get_input) &&
                   (din.vld==1 && pb == i))
        {
            if (c.use_lower[i]) n.head[i] = c.head[i]-1;
            else n.tail[i] = c.tail[i]+1;
        }
    }

    // the wcmd.addr to be output during get_input stage
    sorter_addr_t t1[PART_NUM];
    for (int i=0; i<PART_NUM; i++) {
        t1[i] = c.use_lower[i] ? c.head[i] : c.tail[i];
    }
    sorter_addr_t input_waddr = t1[pb];

    // rotate cnt
    if (c.wcmd_state == WCMD::write_stack)
        n.rot_cnt = c.rot_cnt + 1;
    else
        n.rot_cnt = 0;

    // last_read and last_write
    bool last_read =
        c.rcmd_state == RCMD::sorting &&
        (c.raddrs.lb == c.raddrs.ub);
    bool last_write =
        c.wcmd_state == WCMD::sorting && c.waddrs.lb == c.waddrs.ub;

    // for the stack operation
    bool read_stack, write_stack;
    stack_t stack_input;

    read_stack = c.rcmd_state == RCMD::get_stack || last_read;
    // N.B.: write_stack might be the timing blocker
    write_stack =
        (c.wcmd_state == WCMD::write_stack) ||
        (last_write && (c.waddrs.lb - c.wbounds.lb >= 2)) ||
        (c.ub_stack_vld && (c.ub_stack.ub - c.ub_stack.lb >= 2));
    if (c.wcmd_state == WCMD::write_stack) {
        stack_input.lb = c.head[0];
        stack_input.ub = c.tail[0] - 1;
    } else if (c.ub_stack_vld) {
        stack_input.lb = c.ub_stack.lb + 1;
        stack_input.ub = c.ub_stack.ub;
    } else {
        stack_input.lb = c.wbounds.lb;
        stack_input.ub = c.waddrs.lb - 1;
    }

    // control for pump_stack
    bool read_pump_stack, write_pump_stack;
    stack_t pump_stack_input;
    read_pump_stack = c.rcmd_state == RCMD::get_stack_pump;
    write_pump_stack = c.wcmd_state == WCMD::write_stack;
    pump_stack_input.lb = c.head[0];
    pump_stack_input.ub = c.tail[0]-1;

    // rid: incr whenever get a new stack
    if (reset) n.rid = 0;
    else if (read_stack && stack.has_data()) n.rid = c.rid + 1;

    // rprior
    if (read_stack && stack.has_data()) n.rprior = true;
    else n.rprior = !c.rprior;

    // rbounds
    if (read_stack && stack.has_data()) n.rbounds = stack.top();

    // actually decide read from lower or upper
    bool read_from_lb;
    if (c.write_lower_vld && c.write_lower_id==c.rid)
        read_from_lb = c.write_lower;
    else
        read_from_lb = c.rprior;

    // raddrs
    if (read_stack && stack.has_data()) {
        n.raddrs = stack.top();
    } else if (c.rcmd_state == RCMD::sorting) {
        if (read_from_lb)
            n.raddrs.lb = c.raddrs.lb + 1;
        else
            n.raddrs.ub = c.raddrs.ub - 1;
    }

    // median, wid and wbound
    bool shd_get_median =
        (c.wcmd_state == WCMD::get_median) || last_write;

    if (shd_get_median && mdata.vld==1) {
        n.median = mdata.data;
        n.wid = mdata.id;
        n.wbounds = mdata.init_addrs;
    }

    // waddrs
    bool smaller_than_med = mdata.data.key <= c.median.key;
    if (shd_get_median && mdata.vld==1) {
        n.waddrs = mdata.init_addrs;
    } else {
        if (smaller_than_med) n.waddrs.lb = c.waddrs.lb + 1;
        else n.waddrs.ub = c.waddrs.ub - 1;
    }

    // finish_cnt
    if (c.rcmd_state == RCMD::get_stack) n.finish_cnt = c.finish_cnt+1;
    else n.finish_cnt = 0;

    // pump_head and pump_tail
    if (c.rcmd_state == RCMD::pump_output) {
        n.pump_head = c.pump_head + 1;
    } else {
        n.pump_head = pump_stack.top().lb;
        n.pump_tail = pump_stack.top().ub;
    }

    // ub_stack_vld
    n.ub_stack_vld = last_write;

    // ub_stack
    if (last_write) {
        n.ub_stack.lb = c.waddrs.lb;
        n.ub_stack.ub = c.wbounds.ub;
    }

    // indicated where does we write
    n.write_lower = smaller_than_med;
    n.write_lower_vld = c.wcmd_state == WCMD::sorting;
    n.write_lower_id = c.wid;

    /* The write timing
     *  | wcmd_state     | get_median | sorting | sorting | sorting | sorting | sorting | sorting | get_median |
        | mdata(input)   | 0_0        | 0_1     | 0_2     | 1_0     | 1_1     | 1_2     |         |            |
        | wcmd.data(out) | -          | 0_1     | 0_2     | 0_0     | 1_1     | 1_2     | 1_0     |            |
        | wcmd.vld(out)  | 0          | 1       | 1       | 1       | 1       | 1       | 1       | 0          |
     */

    // write cmd
    mem_wcmd_t wcmd;

    if (c.wcmd_state == WCMD::get_input) {
        wcmd.addr = input_waddr;
        wcmd.data.key = din.key;
        wcmd.data.data = din.data;
        wcmd.both = 0;
    } else {
        wcmd.addr = smaller_than_med ? c.waddrs.lb : c.waddrs.ub;
        wcmd.both = 1;
        if (c.waddrs.lb == c.waddrs.ub) {
            wcmd.data = c.median;
        } else {
            wcmd.data = mdata.data;
        }
    }

    wcmd.vld =
        (c.wcmd_state == WCMD::get_input && din.vld==1) ||
        c.wcmd_state == WCMD::sorting;
    
    // read cmd
    mem_rcmd_t rcmd;

    rcmd.init_addrs = c.rbounds;
    rcmd.id = c.rid;
    if (c.rcmd_state == RCMD::sorting) {
        if (read_from_lb) rcmd.addr = c.raddrs.lb;
        else rcmd.addr = c.raddrs.ub;
        rcmd.pump = 0;
    } else {
        rcmd.addr = c.pump_head;
        rcmd.pump = 1;
    }

    if (c.rcmd_state == RCMD::sorting || c.rcmd_state == RCMD::pump_output)
        rcmd.vld = 1;
    else
        rcmd.vld = 0;

    // assign the output
    mcmd.rcmd = rcmd;
    mcmd.wcmd = wcmd;

    // num_pair
    if (reset==1 || (c.wcmd_state==WCMD::get_median &&
        c.rcmd_state == RCMD::get_stack_pump))
    {
        n.num_pair = 0;
    } else if (din.vld) {
        n.num_pair = c.num_pair + 1;
    }

    // ctrl_out
    ctrl_out.num_pair = c.num_pair;
    ctrl_out.accept_input = c.wcmd_state == WCMD::get_input;
    ctrl_out.pump_done =
        (c.rcmd_state == RCMD::get_stack_pump) &&
        (!pump_stack.has_data());

    // WCMD state machine
    switch (c.wcmd_state)
    {
    case WCMD::get_input:
        if (din_done) n.wcmd_state = WCMD::wait_pump_finish;
        break;

    case WCMD::wait_pump_finish:
        if (c.rcmd_state == RCMD::wait_input_finish)
            n.wcmd_state = WCMD::write_stack;
        break;

    case WCMD::write_stack:
        if (c.rot_cnt == num_t(-1)) {
            n.wcmd_state = WCMD::get_median;
        }
        break;
    
    case WCMD::get_median:
        if (mdata.vld==1)
            n.wcmd_state = WCMD::sorting;
        else if (c.rcmd_state == RCMD::get_stack_pump)
            n.wcmd_state = WCMD::get_input;
        break;

    case WCMD::sorting:
        if (c.waddrs.lb == c.waddrs.ub && mdata.vld == 0)
            n.wcmd_state = WCMD::get_median;
        break;

    default:
        break;
    }
    if (reset) n.wcmd_state = WCMD::get_input;

    // RCMD state machine
    switch (c.rcmd_state)
    {
    case RCMD::wait_input_finish:
        if (c.wcmd_state == WCMD::wait_pump_finish)
            n.rcmd_state = RCMD::get_stack;
        break;
    
    case RCMD::get_stack:
        if (stack.has_data())
            n.rcmd_state = RCMD::sorting;
        else if (c.finish_cnt == finish_t(-1))
            n.rcmd_state = RCMD::get_stack_pump;
        break;

    case RCMD::sorting:
        if (c.raddrs.lb == c.raddrs.ub && (!stack.has_data()))
            n.rcmd_state = RCMD::get_stack;
        break;

    case RCMD::get_stack_pump:
        if (pump_stack.has_data())
            n.rcmd_state = RCMD::pump_output;
        else
            n.rcmd_state = RCMD::wait_input_finish;
        break;
    
    case RCMD::pump_output:
        if (c.pump_head == c.pump_tail)
            n.rcmd_state = RCMD::get_stack_pump;
        break;
    
    default:
        break;
    }
    if (reset) n.rcmd_state = RCMD::wait_input_finish;

    // the stack operation
    stack.process_once(reset, read_stack, write_stack, stack_input);
    pump_stack.process_once(reset, read_pump_stack,
        write_pump_stack, pump_stack_input);

    c = n;
}
};
