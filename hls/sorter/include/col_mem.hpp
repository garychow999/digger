#pragma once
#include <top.hpp>
#include <shifter.hpp>

class Col_mem {
public:

    // internal data type
    using int_t = ap_uint< (136+16) * 8 >;

    enum class Int_State : unsigned {
        store_input,
        pumping
    };

    struct State {

        Int_State int_state;
        collector_id_t current_bank;

        col_mux_addr_t head[COLLECTOR_GROUP];
        col_mux_addr_t tail[COLLECTOR_GROUP];
        collector_t mout[COLLECTOR_GROUP];

        // the buffered data
    };

    State c;
    collector_t mem[COLLECTOR_GROUP][1 << COL_MUX_AW];

    Col_mem() {
    }

void process_once(
    vld_t reset,
    main_ctrl_t main_ctrl,
    collector_t din[COLLECTOR_GROUP],
    vld_t dout_ready,
    keccak_t& dout
) {
    auto n = c;

    // read
    for (int i=0; i<COLLECTOR_GROUP; i++)
        n.mout[i] = mem[i][c.head[i]];

    // write and tail
    for (int i=0; i<COLLECTOR_GROUP; i++)
    {
        mem[i][c.tail[i]] = din[i];
        if (din[i].vld)
            n.tail[i] = c.tail[i] + 1;
        else if (reset | main_ctrl.reset_col_mux)
            n.tail[i] = 0;
    }

    // state mcahine
    switch (c.int_state)
    {
    case Int_State::store_input:
        break;

    case Int_State::pumping:
        break;
    
    default:
        break;
    }
    if (reset | main_ctrl.reset_col_mux) n.int_state = Int_State::store_input;
    
    c = n;
}
};
