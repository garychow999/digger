#pragma once
#include <top.hpp>

template <const int LEVEL=0, const int LAYER_PIPELINE=10>
class Mux {
public:
    static const int AW = 5;
    static const int NUM = 1 << AW;
    static const int THRESHOLD = NUM - LAYER_PIPELINE;
    using ptr_t = ap_uint<AW>;

    // q memory
    sorter_key_data_t q[2][NUM];
    ptr_t head[2];
    ptr_t tail[2];
    bool pri;

    Mux() {
        #pragma HLS BIND_STORAGE variable=q type=RAM_S2P impl=LUTRAM
        #pragma HLS aggregate variable=q compact=bit
    }

void process_once(
    vld_t reset,
    vld_t sel_bit,
    sorter_key_data_t din[2],
    sorter_key_data_t& dout,
    vld_t fin,
    U2& fout
) {
    // fout
    for (int i=0; i<2; i++) {
        ptr_t space = head[i] - tail[i];
        space--;
        fout[i] = space > THRESHOLD;
    }

    // q read
    sorter_key_data_t qout[2];
    bool has[2];
    bool read[2];
    for (int i=0; i<2; i++) {
        qout[i] = q[i][head[i]];
        has[i] = head[i] != tail[i];
    }
    read[0] = fin==0 && ((has[0] && !has[1]) || (has[0] && has[1] && pri));
    read[1] = fin==0 && ((has[1] && !has[0]) || (has[1] && has[0] && !pri));
    pri = !pri;
    for (int i=0; i<2; i++) if (read[i]) head[i]++;

    // dout
    dout = read[0] ? qout[0] : qout[1];
    dout.vld = read[0] || read[1];

    // q write
    for (int i=0; i<2; i++) {
        U1 bit = din[i].key[SORTER_KEY_WIDTH-1-LEVEL];
        q[i][tail[i]] = din[i];
        if (bit == sel_bit) tail[i]++;
    }

    if (reset) {
        for (int i=0; i<2; i++) head[i] = tail[i] = 0;
        pri = true;
    }
}
};