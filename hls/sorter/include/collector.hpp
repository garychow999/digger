// collect 1-byte from sorter memory to 16-byte memory

#pragma once
#include <top.hpp>

#include <shifter.hpp>

class Collector {
public:
    struct State {
        collector_t data;
    };

    State c;

void process_once(
    vld_t reset,
    collector_id_t const_collector_id,
    collector_id_t active_collector_id,
    mem_pump_data_t din,
    collector_t& dout
) {
    auto n = c;

    if (reset) {
        dout = collector_t::zeros();
        n.data = collector_t::zeros();
    } else {
        // output
        if (const_collector_id == active_collector_id) {
            dout = c.data;
            dout.id = const_collector_id;
            n.data = collector_t::zeros();
        } else {
            dout = collector_t::zeros();
        }

        // data input --> internal buffer
        if (din.vld) {
            n.data.data = (din.data, c.data.data) >> 8;
            n.data.be = n.data.be + 1;
            n.data.vld = 1;
        }
    }

    c = n;
}
};