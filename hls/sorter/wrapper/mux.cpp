#include <mux.hpp>

void mux0(
    vld_t reset,
    vld_t sel_bit,
    sorter_key_data_t din0,
    sorter_key_data_t din1,
    sorter_key_data_t& dout,
    vld_t fin,
    U2& fout
) {
    #pragma HLS PIPELINE II=1 style=frp
    #pragma HLS inline recursive
    #pragma HLS INTERFACE ap_ctrl_none port=return

    #pragma HLS INTERFACE ap_none port=reset
    #pragma HLS stable variable=sel_bit
    #pragma HLS INTERFACE ap_none port=din0
    #pragma HLS INTERFACE ap_none port=din1
    #pragma HLS INTERFACE ap_none port=dout
    #pragma HLS INTERFACE ap_none port=fin
    #pragma HLS INTERFACE ap_none port=fout

    #pragma HLS aggregate variable=din0 compact=bit
    #pragma HLS aggregate variable=din1 compact=bit
    #pragma HLS aggregate variable=dout compact=bit

    sorter_key_data_t din[2];
    din[0] = din0;
    din[1] = din1;

    static Mux<0> mux;
    mux.process_once(reset, sel_bit, din, dout, fin, fout);
}
