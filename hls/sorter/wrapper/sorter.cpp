#define PART_AW 3
#include <sorter.hpp>

void sorter(
    vld_t reset,
    sorter_key_data_t din,
    vld_t din_done,
    mem_data_t mdata,
    mem_cmd_t& mcmd,
    sorter_ctrl_out_t& ctrl_out
) {
    #pragma HLS PIPELINE II=1 style=frp
    #pragma HLS inline recursive
    #pragma HLS INTERFACE ap_ctrl_none port=return

    #pragma HLS INTERFACE ap_none port=reset
    #pragma HLS INTERFACE ap_none port=din
    #pragma HLS INTERFACE ap_none port=din_done
    #pragma HLS INTERFACE ap_none port=mdata
    #pragma HLS INTERFACE ap_none port=mcmd
    #pragma HLS INTERFACE ap_none port=ctrl_out

    #pragma HLS aggregate variable=din compact=bit
    #pragma HLS aggregate variable=mdata compact=bit
    #pragma HLS aggregate variable=mcmd compact=bit
    #pragma HLS aggregate variable=ctrl_out compact=bit

    static Sorter sorter;
    sorter.process_once(
        reset,
        din,
        din_done,
        mdata,
        mcmd,
        ctrl_out
    );
}