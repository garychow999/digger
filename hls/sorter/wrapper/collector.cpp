#include <collector.hpp>

void collector(
    vld_t reset,
    collector_id_t const_collector_id,
    collector_id_t active_collector_id,
    mem_pump_data_t din,
    collector_t& dout
) {
    #pragma HLS PIPELINE II=1 style=frp
    #pragma HLS inline recursive
    #pragma HLS INTERFACE ap_ctrl_none port=return

    #pragma HLS INTERFACE ap_none port=reset
    #pragma HLS INTERFACE ap_none port=const_collector_id
    #pragma HLS INTERFACE ap_none port=active_collector_id
    #pragma HLS INTERFACE ap_none port=din
    #pragma HLS INTERFACE ap_none port=dout

    #pragma HLS aggregate compact=bit variable=const_collector_id
    #pragma HLS aggregate compact=bit variable=active_collector_id
    #pragma HLS aggregate compact=bit variable=din
    #pragma HLS aggregate compact=bit variable=dout

    static Collector collector;
    collector.process_once(
        reset,
        const_collector_id,
        active_collector_id,
        din,
        dout
    );
}