# get the parameters from Makefile
set device      [lindex $argv 2]
set period      [lindex $argv 3]
set clk_uncert  [lindex $argv 4]

# open the project
open_project    -reset syn_mux

# design files
add_files       ../../sorter/wrapper/mux.cpp -cflags "-I../../sorter/include -I../../../interface/sw_include -I../../../interface/hw_include -std=c++17"

# set top
set_top     mux0
# open solution
open_solution   "solution1"

# some options
config_compile  -name_max_length 1000

# parts
set_part $device

# optimization

# clock
create_clock -period    $period -name clk1
set_clock_uncertainty   $clk_uncert%

# synthesis
csynth_design

exit
