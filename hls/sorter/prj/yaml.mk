syn_mux: syn_report
	vitis_hls -f ./tcl/syn_mux.tcl ${device} ${period} ${clk_uncert} -l vitis_hls_syn_mux.log
	cp syn_mux/solution1/syn/report/*.rpt syn_report

syn_sorter: syn_report
	vitis_hls -f ./tcl/syn_sorter.tcl ${device} ${period} ${clk_uncert} -l vitis_hls_syn_sorter.log
	cp syn_sorter/solution1/syn/report/*.rpt syn_report

syn_collector: syn_report
	vitis_hls -f ./tcl/syn_collector.tcl ${device} ${period} ${clk_uncert} -l vitis_hls_syn_collector.log
	cp syn_collector/solution1/syn/report/*.rpt syn_report

impl_mux: syn_mux
	vitis_hls -f ./tcl/impl_mux.tcl -l vitis_hls_impl_mux.log

impl_sorter: syn_sorter
	vitis_hls -f ./tcl/impl_sorter.tcl -l vitis_hls_impl_sorter.log

impl_collector: syn_collector
	vitis_hls -f ./tcl/impl_collector.tcl -l vitis_hls_impl_collector.log

syn_report:
	mkdir -p syn_report

yaml_sim:

yaml_syn: syn_mux syn_sorter syn_collector

yaml_impl: impl_mux impl_sorter impl_collector

yaml_clean:
	rm -rf *.pcap syn_mux syn_sorter syn_collector impl_mux impl_sorter impl_collector
	@echo '    syn_mux'
	@echo '    syn_sorter'
	@echo '    syn_collector'


