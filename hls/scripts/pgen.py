#!/usr/bin/env python3

import os
import shutil
import yaml

"""
Thing to consider:
1. Perhaps add one more type
"""

projects = dict()
projects['sim'] = []
projects['syn'] = []

def get_tcl(p, prj_name):
    results = []

    # header part
    results.append('# get the parameters from Makefile')
    results.append('set device      [lindex $argv 2]')
    results.append('set period      [lindex $argv 3]')
    results.append('set clk_uncert  [lindex $argv 4]')
    results.append('')
    results.append('# open the project')
    results.append('open_project    -reset {0}'.format(prj_name))
    results.append('')

    # body part
    cflag = ''
    if 'inc' in p:
        for i in p['inc']:
            cflag = cflag + '-I{0} '.format(i)
    cflag = cflag + '-std=c++17'

    results.append('# design files')
    if 'files' in p:
        for f in p['files']:
            results.append('add_files       {0} -cflags "{1}"'.format(f, cflag))
    results.append('')

    if 'tb' in p:
        results.append('# test bench')
        for tb in p['tb']:
            results.append('add_files -tb   {0} -cflags "{1}" -csimflags "-Wno-unknown-pragmas -Wno-subobject-linkage "'.format(tb, cflag))
        results.append('')

    if 'top' in p:
        results.append('# set top')
        results.append('set_top     {0}'.format(p['top']))

    # footer part
    results.append('# open solution')
    results.append('open_solution   "solution1"')
    results.append('')

    results.append('# some options')
    results.append('config_compile  -name_max_length 1000')
    results.append('')

    results.append('# parts')
    # results.append('set_part $device -tool vitis')
    results.append('set_part $device')
    results.append('')

    results.append('# optimization')
    #results.append('config_bind -effort high')
    # results.append('config_schedule -effort medium -relax_ii_for_timing=0')
    results.append('')

    ldflags = ''
    if 'ldflags' in p:
        ldflags = p['ldflags']

    argv = ''
    if 'argv' in p:
        argv = p['argv']

    if p['type'] == 'syn':
        results.append('# clock')
        results.append('create_clock -period    $period -name clk1')
        results.append('set_clock_uncertainty   $clk_uncert%')
        results.append('')
        results.append('# synthesis')
        results.append('csynth_design')
        results.append('')

    if p['type'] == 'sim':
        results.append('# simulation')
        results.append('csim_design -ldflags {{{0}}} -argv {{{1}}} -O'.format(ldflags, argv))
        results.append('')

    results.append('exit')

    # add new line
    return [s + '\n' for s in results]


def get_impl_tcl(p):
    results = []
    results.append('# open the project')
    results.append('open_project\t\tsyn_{0}'.format(p['name']))
    results.append('open_solution\t\t"solution1"')
    results.append('')
    results.append('export_design -rtl verilog -format ip_catalog -vendor "vendor" -display_name "{0}"'
                   .format(p['name']))
    results.append('')
    results.append('exit')

    # add new line
    return [s + '\n' for s in results]


def add_prj(p):
    global projects

    type1 = p['type']

    # check the project type
    if not(type1 == 'syn' or type1 == 'sim'):
        print('type={0} error'.format(type1))
        exit(1)

    # check if we have a duplicated project
    prj_name = p['type'] + '_' + p['name']
    if prj_name in projects[type1]:
        print('Error, found duplicated project = {0}'.format(prj_name))
        exit(0)

    # make the dir if it does not exist
    if not os.path.exists('./tcl'):
        os.makedirs('tcl')

    projects[type1].append(p['name'])

    # get and write the tcl
    tcl = get_tcl(p, prj_name)
    tcl_name = './tcl/{0}.tcl'.format(prj_name)

    with open(tcl_name, 'w+') as f:
        f.writelines(tcl)
        f.close()

    # add the impl tcl if it is syn project
    if p['type'] == 'syn':
        tcl = get_impl_tcl(p)
        tcl_name = './tcl/impl_{0}.tcl'.format(p['name'])

    with open(tcl_name, 'w+') as f:
        f.writelines(tcl)
        f.close()


def add_to_make(results:list, name:str, syn=False):
    type = 'sim'
    if syn:
        type = 'syn'

    if syn:
        results.append('{0}_{1}: syn_report'.format(type, name))
    else:
        results.append('{0}_{1}:'.format(type, name))

    results.append('\tvitis_hls -f ./tcl/{0}_{1}.tcl ${{device}} ${{period}} ${{clk_uncert}} -l vitis_hls_{0}_{1}.log'
                   .format(type, name))
    if syn:
        # add the copying
        results.append('\tcp syn_{0}/solution1/syn/report/*.rpt syn_report'.format(name))
    results.append('')


# TODO: need to add datetime stamp on the zip file...
def add_impl_to_make(results:list, name:str):
    # add the impl script
    results.append('impl_{0}: syn_{0}'.format(name))
    results.append('\tvitis_hls -f ./tcl/impl_{0}.tcl -l vitis_hls_impl_{0}.log'.format(name))
    #results.append('\tcp syn_{0}/solution1/impl/ip/*.zip ${{HLS_ROOT}}/ip_repo'.format(name))
    results.append('')


def gen_makefile():
    global projects
    results = []

    prj_line = ''
    yaml_syn_line = 'yaml_syn:'
    yaml_sim_line = 'yaml_sim:'
    yaml_impl_line = 'yaml_impl:'
    yaml_clean_line = '\trm -rf *.pcap'

    for p in projects['sim']:
        add_to_make(results, p)
        yaml_sim_line = yaml_sim_line + ' sim_{0}'.format(p)
        yaml_clean_line = yaml_clean_line + ' sim_{0}'.format(p)
        prj_line = prj_line + '\t@echo \'    sim_{0}\'\n'.format(p)

    for p in projects['syn']:
        add_to_make(results, p, True)
        yaml_syn_line = yaml_syn_line + ' syn_{0}'.format(p)
        yaml_clean_line = yaml_clean_line + ' syn_{0}'.format(p)
        prj_line = prj_line + '\t@echo \'    syn_{0}\'\n'.format(p)

    for p in projects['syn']:
        add_impl_to_make(results, p)
        yaml_impl_line = yaml_impl_line + ' impl_{0}'.format(p)
        yaml_clean_line = yaml_clean_line + ' impl_{0}'.format(p)


    # add the line to create dir
    results.append('syn_report:')
    results.append('\tmkdir -p syn_report')
    results.append('')

    # add the yaml_syn
    results.append(yaml_sim_line)
    results.append('')

    results.append(yaml_syn_line)
    results.append('')

    results.append(yaml_impl_line)
    results.append('')

    results.append('yaml_clean:')
    results.append(yaml_clean_line)
    results.append(prj_line)
    results.append('')

    # add new line
    results = map(lambda x: x + '\n', results)

    # write to the yaml.mk file
    with open('yaml.mk', 'w+') as f:
        f.writelines(results)
        f.close()


# =================================================================================
# clean up the old stuff
try:
    shutil.rmtree('./tcl')
    os.remove('yaml.mk')
except:
    pass

# load the yaml
with open('prj.yaml', 'r') as f:
    temp = yaml.load(f, Loader=yaml.FullLoader)
    jsrc = temp['projects']


# generate tcls for all the projects
for p in jsrc:
    # it is only a project if it has a name
    if 'name' in p:
        if p['type'] == 'sim_syn':
            p['type'] = 'sim'
            add_prj(p)
            p['type'] = 'syn'
            add_prj(p)
        else:
            add_prj(p)

# generate the make file
gen_makefile()



