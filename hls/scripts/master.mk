# setup the device and the clk
device 		?= "xcvu3p-ffvc1517-2-e"
period 		?= 3.1
clk_uncert 	?= 20

all: sim syn

sim: yaml_sim

syn: yaml_syn

gen:
	../../scripts/pgen.py

clean: yaml_clean
	rm -rf *.log vivado* syn_report
