#include <ap_int.h>

void be_memory(
    ap_uint<32> din0,
    ap_uint<1> din0_vld,
    ap_uint<8> din1,
    ap_uint<1> din1_vld,
    ap_uint<12> waddr,
    ap_uint<12> raddr,
    ap_uint<32>& dout0,
    ap_uint<8>& dout1
) {
    #pragma HLS PIPELINE II=1 style=frp
    #pragma HLS inline recursive
    #pragma HLS INTERFACE ap_ctrl_none port=return

    #pragma HLS INTERFACE ap_none port=din0
    #pragma HLS INTERFACE ap_none port=din0_vld
    #pragma HLS INTERFACE ap_none port=din1
    #pragma HLS INTERFACE ap_none port=din1_vld

    #pragma HLS INTERFACE ap_none port=waddr
    #pragma HLS INTERFACE ap_none port=raddr

    #pragma HLS INTERFACE ap_none port=dout0
    #pragma HLS INTERFACE ap_none port=dout1

    // the memory
    static ap_uint<32> mem0[1<<12];
    static ap_uint<8> mem1[1<<12];
    #pragma HLS BIND_STORAGE variable=mem0 type=ram_s2p impl=uram latency=1
    #pragma HLS BIND_STORAGE variable=mem1 type=ram_s2p impl=uram latency=1

    // read
    dout0 = mem0[raddr];
    dout1 = mem1[raddr];

    // write
    if (din0_vld) {
        mem0[waddr] = din0;
    }

    if (din1_vld) {
        mem1[waddr] = din1;
    }
 
}
