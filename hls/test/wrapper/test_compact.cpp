#include <top.hpp>

typedef struct{
    U7 R, G;
    // unsigned char R, G, B;
} Pixel;

void test_compact(vld_t sel,Pixel din0, Pixel din1, Pixel& dout) {

    // pipelining and top function ctrl
    #pragma HLS PIPELINE II=1 style=frp
    #pragma HLS inline recursive
    #pragma HLS INTERFACE ap_ctrl_none  port=return

    // ap_none interface
    #pragma HLS INTERFACE ap_none       port=din0
    #pragma HLS INTERFACE ap_none       port=din1
    #pragma HLS INTERFACE ap_none       port=dout

    // aggregate interface
    #pragma HLS aggregate variable=din0 compact=bit
    #pragma HLS aggregate variable=din1 compact=bit
    #pragma HLS aggregate variable=dout compact=bit

    if (sel==0) {
        dout.R = din0.G;
        dout.G = din0.R;
    } else {
        dout.R = din1.G;
        dout.G = din1.R;
    }
}