#include <ap_int.h>

class Test_array {
public:

    ap_uint<32> data[16];

    Test_array() {
        // #pragma HLS BIND_STORAGE variable=data type=ram_s2p impl=uram latency=1
        #pragma HLS array_partition variable=data
        for (int i=0; i<16; i++) data[i] = i+1;
    }

void process_once(
    ap_uint<4> waddr,
    ap_uint<32> wdata,
    ap_uint<4> raddr,
    ap_uint<32>& rdata
) {

    // read
    rdata = data[raddr];

    // write
    data[waddr] = wdata;
}
};

void test_array(
    ap_uint<4> waddr,
    ap_uint<32> wdata,
    ap_uint<4> raddr,
    ap_uint<32>& rdata
) {
    #pragma HLS PIPELINE II=1 style=frp
    #pragma HLS inline recursive
    #pragma HLS INTERFACE ap_ctrl_none port=return

    #pragma HLS INTERFACE ap_none port=waddr
    #pragma HLS INTERFACE ap_none port=wdata
    #pragma HLS INTERFACE ap_none port=raddr
    #pragma HLS INTERFACE ap_none port=rdata

    static Test_array t;
    t.process_once(
        waddr, wdata, raddr, rdata
    );
}

