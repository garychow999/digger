#include <top.hpp>

void test_lut_latency(
    U24 din,
    vld_t din_vld,
    vld_t read,
    U24& dout
) {
    // pipelining and top function ctrl
    #pragma HLS PIPELINE II=1 style=frp
    #pragma HLS inline recursive
    #pragma HLS INTERFACE ap_ctrl_none  port=return

    // ap_none interface
    #pragma HLS INTERFACE ap_none       port=din
    #pragma HLS INTERFACE ap_none       port=din_vld
    #pragma HLS INTERFACE ap_none       port=read
    #pragma HLS INTERFACE ap_none       port=dout

    static U24 data[32];
    static U5 ptr = 0;
    static U5 ptr_m1 = -1;

    



}