#include <ap_int.h>

struct Test1 {
    ap_uint<10> field0;
    ap_uint<10> field1;
};

void test_array_intf(
    Test1 dout[2]
) {
    #pragma HLS PIPELINE II=1 style=frp
    #pragma HLS inline recursive
    #pragma HLS INTERFACE ap_ctrl_none port=return

    #pragma HLS INTERFACE ap_none port=dout

    dout[0].field0 = 10;
    dout[0].field1 = 20;

    dout[1].field0 = 30;
    dout[1].field1 = 40;
}