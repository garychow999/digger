syn_test_compact: syn_report
	vitis_hls -f ./tcl/syn_test_compact.tcl ${device} ${period} ${clk_uncert} -l vitis_hls_syn_test_compact.log
	cp syn_test_compact/solution1/syn/report/*.rpt syn_report

syn_test_array: syn_report
	vitis_hls -f ./tcl/syn_test_array.tcl ${device} ${period} ${clk_uncert} -l vitis_hls_syn_test_array.log
	cp syn_test_array/solution1/syn/report/*.rpt syn_report

syn_be_memory: syn_report
	vitis_hls -f ./tcl/syn_be_memory.tcl ${device} ${period} ${clk_uncert} -l vitis_hls_syn_be_memory.log
	cp syn_be_memory/solution1/syn/report/*.rpt syn_report

syn_test_array_intf: syn_report
	vitis_hls -f ./tcl/syn_test_array_intf.tcl ${device} ${period} ${clk_uncert} -l vitis_hls_syn_test_array_intf.log
	cp syn_test_array_intf/solution1/syn/report/*.rpt syn_report

impl_test_compact: syn_test_compact
	vitis_hls -f ./tcl/impl_test_compact.tcl -l vitis_hls_impl_test_compact.log

impl_test_array: syn_test_array
	vitis_hls -f ./tcl/impl_test_array.tcl -l vitis_hls_impl_test_array.log

impl_be_memory: syn_be_memory
	vitis_hls -f ./tcl/impl_be_memory.tcl -l vitis_hls_impl_be_memory.log

impl_test_array_intf: syn_test_array_intf
	vitis_hls -f ./tcl/impl_test_array_intf.tcl -l vitis_hls_impl_test_array_intf.log

syn_report:
	mkdir -p syn_report

yaml_sim:

yaml_syn: syn_test_compact syn_test_array syn_be_memory syn_test_array_intf

yaml_impl: impl_test_compact impl_test_array impl_be_memory impl_test_array_intf

yaml_clean:
	rm -rf *.pcap syn_test_compact syn_test_array syn_be_memory syn_test_array_intf impl_test_compact impl_test_array impl_be_memory impl_test_array_intf
	@echo '    syn_test_compact'
	@echo '    syn_test_array'
	@echo '    syn_be_memory'
	@echo '    syn_test_array_intf'


