#include <stdint.h>
#include <string.h>

#define ROTL64(x, n)		(((x) << (n)) | ((x) >> (64 - (n))))

const uint64_t keccakf_rnd_consts[24] = 
{
    0x0000000000000001ULL, 0x0000000000008082ULL, 0x800000000000808aULL,
    0x8000000080008000ULL, 0x000000000000808bULL, 0x0000000080000001ULL,
    0x8000000080008081ULL, 0x8000000000008009ULL, 0x000000000000008aULL,
    0x0000000000000088ULL, 0x0000000080008009ULL, 0x000000008000000aULL,
    0x000000008000808bULL, 0x800000000000008bULL, 0x8000000000008089ULL,
    0x8000000000008003ULL, 0x8000000000008002ULL, 0x8000000000000080ULL, 
    0x000000000000800aULL, 0x800000008000000aULL, 0x8000000080008081ULL,
    0x8000000000008080ULL, 0x0000000080000001ULL, 0x8000000080008008ULL
};

void keccakf(uint64_t *st)
{
    for(int i = 0; i < 24; ++i) 
    {
		uint64_t bc[5], tmp;

		// Theta
		bc[0] = st[4] ^ st[9] ^ st[14] ^ st[19] ^ st[24] ^ ROTL64(st[1] ^ st[6] ^ st[11] ^ st[16] ^ st[21], 1); 
		bc[1] = st[0] ^ st[5] ^ st[10] ^ st[15] ^ st[20] ^ ROTL64(st[2] ^ st[7] ^ st[12] ^ st[17] ^ st[22], 1); 
		bc[2] = st[1] ^ st[6] ^ st[11] ^ st[16] ^ st[21] ^ ROTL64(st[3] ^ st[8] ^ st[13] ^ st[18] ^ st[23], 1); 
		bc[3] = st[2] ^ st[7] ^ st[12] ^ st[17] ^ st[22] ^ ROTL64(st[4] ^ st[9] ^ st[14] ^ st[19] ^ st[24], 1); 
		bc[4] = st[3] ^ st[8] ^ st[13] ^ st[18] ^ st[23] ^ ROTL64(st[0] ^ st[5] ^ st[10] ^ st[15] ^ st[20], 1); 
		st[0] ^= bc[0]; 

		// Rho & Pi
		tmp = ROTL64(st[ 1] ^ bc[1], 1); 
		st[ 1] = ROTL64(st[ 6] ^ bc[1], 44); 
		st[ 6] = ROTL64(st[ 9] ^ bc[4], 20); 
		st[ 9] = ROTL64(st[22] ^ bc[2], 61); 
		st[22] = ROTL64(st[14] ^ bc[4], 39); 
		st[14] = ROTL64(st[20] ^ bc[0], 18); 
		st[20] = ROTL64(st[ 2] ^ bc[2], 62); 
		st[ 2] = ROTL64(st[12] ^ bc[2], 43); 
		st[12] = ROTL64(st[13] ^ bc[3], 25); 
		st[13] = ROTL64(st[19] ^ bc[4],  8); 
		st[19] = ROTL64(st[23] ^ bc[3], 56); 
		st[23] = ROTL64(st[15] ^ bc[0], 41); 
		st[15] = ROTL64(st[ 4] ^ bc[4], 27); 
		st[ 4] = ROTL64(st[24] ^ bc[4], 14); 
		st[24] = ROTL64(st[21] ^ bc[1],  2); 
		st[21] = ROTL64(st[ 8] ^ bc[3], 55); 
		st[ 8] = ROTL64(st[16] ^ bc[1], 45); 
		st[16] = ROTL64(st[ 5] ^ bc[0], 36); 
		st[ 5] = ROTL64(st[ 3] ^ bc[3], 28); 
		st[ 3] = ROTL64(st[18] ^ bc[3], 21); 
		st[18] = ROTL64(st[17] ^ bc[2], 15); 
		st[17] = ROTL64(st[11] ^ bc[1], 10); 
		st[11] = ROTL64(st[ 7] ^ bc[2],  6); 
		st[ 7] = ROTL64(st[10] ^ bc[0],  3); 
		st[10] = tmp; 

		// Chi
		bc[0] = st[ 0]; bc[1] = st[ 1]; st[ 0] ^= (~bc[1]) & st[ 2]; st[ 1] ^= (~st[ 2]) & st[ 3]; st[ 2] ^= (~st[ 3]) & st[ 4]; st[ 3] ^= (~st[ 4]) & bc[0]; st[ 4] ^= (~bc[0]) & bc[1]; 
		bc[0] = st[ 5]; bc[1] = st[ 6]; st[ 5] ^= (~bc[1]) & st[ 7]; st[ 6] ^= (~st[ 7]) & st[ 8]; st[ 7] ^= (~st[ 8]) & st[ 9]; st[ 8] ^= (~st[ 9]) & bc[0]; st[ 9] ^= (~bc[0]) & bc[1]; 
		bc[0] = st[10]; bc[1] = st[11]; st[10] ^= (~bc[1]) & st[12]; st[11] ^= (~st[12]) & st[13]; st[12] ^= (~st[13]) & st[14]; st[13] ^= (~st[14]) & bc[0]; st[14] ^= (~bc[0]) & bc[1]; 
		bc[0] = st[15]; bc[1] = st[16]; st[15] ^= (~bc[1]) & st[17]; st[16] ^= (~st[17]) & st[18]; st[17] ^= (~st[18]) & st[19]; st[18] ^= (~st[19]) & bc[0]; st[19] ^= (~bc[0]) & bc[1]; 
		bc[0] = st[20]; bc[1] = st[21]; st[20] ^= (~bc[1]) & st[22]; st[21] ^= (~st[22]) & st[23]; st[22] ^= (~st[23]) & st[24]; st[23] ^= (~st[24]) & bc[0]; st[24] ^= (~bc[0]) & bc[1]; 

		// Iota
		st[0] ^= keccakf_rnd_consts[i];
    }
}

// This assumes a fixed input of 76 bytes, and a fixed output size of 200 bytes.
void keccak(const uint8_t *in, uint8_t *md)
{
    uint64_t st[25] __attribute ((aligned(32)));
    int i = 76;
	
    memcpy(st, in, 76);
    ((uint8_t *)st)[i++] = 1;
    memset(&((uint8_t *)st)[i], 0, sizeof(st) - i);
    ((uint8_t *)st)[135] |= 0x80;

    keccakf(st);

    memcpy(md, st, 200);
}
