#pragma once

#include <ap_int.h>

template<const int AW, const int BW, const bool right=true>
ap_uint<BW*8>byte_shift(
    ap_uint<AW> ctrl,
    ap_uint<BW*8> din
) {
    auto ret = din;
    for (int i=0; i<AW; i++) {
        if (ctrl[i]==1) {
            if (right) ret = ret >> (1<<i) * 8;
            else ret = ret << (1<<i) * 8;
        }
    }
    return ret;
}
