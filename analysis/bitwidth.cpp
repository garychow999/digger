// calculate the probablity of collision using the birthday problem

#include <cstdio>
#include <cstdlib>
#include <set>

#define N 9973
#define ITER 10000

char data[N+8];

int main()
{
    for (int bit=28; bit<=40; bit++) {

        // prepare the mask
        unsigned long long mask = -1;
        mask = mask >> bit;
        mask = mask << bit;
        mask = ~mask;
        // printf("mask = %llX\n", mask);

        double collision = 0;

        for (int i=0; i<ITER; i++) {
            // init the random data
            for (int j=0; j<N+8; j++)
                data[j] = random();
            // check collision
            std::set<unsigned long long> hash;
            for (int j=0; j<N; j++) {
                unsigned long long* ptr = (unsigned long long*)(data+j);
                unsigned long long val = (*ptr) & mask;
                if (hash.find(val) != hash.end()) {
                    collision += 1.0;
                    break;
                }
                hash.insert(val);
            }
        }
        printf("bit=%2d, collision=%lf\n", bit, collision/ITER);

    }

    return 0;
}
