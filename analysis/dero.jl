using SpecialFunctions

# calculate the possibility of inside x sd, given num_bucket
function inside_x_sd(x, num_bucket)
    a = 0.5 + (erf(x/sqrt(2)) / 2)
    a^(num_bucket * 128)
end
