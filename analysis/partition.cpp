// this file is no longer needed

#include <cstdio>
#include <cstdlib>
#include <set>

#define N (128*2000)
#define BUCKET 20

char data[N+8];
unsigned long long int thresholds[BUCKET];

int main(int argc, char** argv)
{
    int ITER = atoi(argv[1]);

    double collision = 0;
    int min_buffer = 10000;

    // set the thresholds
    unsigned long long int each = -1;
    each = each / BUCKET;
    for (int i=0; i<BUCKET; i++)
        thresholds[i] = (i+1) * each;

    for (int iter=0; iter<ITER; iter++)
    {
        // init the data
        for (int i=0; i<N+8; i++)
            data[i] = random();

        // reset the buckets
        int buckets[BUCKET];
        for (int i=0; i<BUCKET; i++)
            buckets[i] = 0;

        // checkout the data one by one
        for (int i=0; i<N; i++)
        {
            unsigned long long int *val = (unsigned long long int*)(data+i);
            int bucket_id = 0;
            for (; bucket_id<BUCKET; bucket_id++)
            {
                if (*val <= thresholds[bucket_id])
                    break;
            }

            buckets[bucket_id]++;
            if (buckets[bucket_id] > 4096)
            {
                collision++;
                break;
            }
        }

        // show the progress
        int max = 0;
        for (int i=0; i<BUCKET; i++)
            max = buckets[i] > max ? buckets[i] : max;

        int buffer = 4096 - max;
        printf("iter=%d, buffer=%d\n", iter, buffer);

        // update the min buffer
        min_buffer = min_buffer > buffer ? buffer : min_buffer;
    }

    printf("collision rate = %lf\n", collision / ITER);
    printf("min buffer = %d\n", min_buffer);

    return 0;
}
